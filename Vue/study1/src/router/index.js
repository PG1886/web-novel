import Vue from 'vue'
import VueRouter from 'vue-router'
// import App from '.././App.vue'
import Index from '../views/Layout/MyMenu.vue'

Vue.use(VueRouter)

const routes = [
  {
    path:'/index/view',
    component:() => import('@/views/Home/Data.vue')
},
    {
        path:'/index',
        redirect:'',
        component:() => import('@/views/Layout/index.vue'),
        children: [
           
        ]
    },
  {
    path: '',
    component:Index,
    redirect:'/index/view',
  },
{
    path:'/index/user/info',
    component: () => import('@/views/User/User.vue')
},
// {
//   path:'/index/user/complaint',
//   component: () => import('@/views/User/findPassword.vue')
// },

{
  path:'/index/author/info',
  component:() => import('@/views/author/authorInfo.vue')
},

{
  path:'/index/author/application',
  component:() => import('@/views/author/authorApplication.vue')
},

{
  path:'/index/book/info',
  component:() => import('@/views/book/bookInfo.vue')
},

{
  path:'/index/book/application',
  component:() => import('@/views/book/bookApplication.vue')
},

{
  path:'/index/book/get',
  component:() => import('@/views/book/getBook.vue')
},

{
  path:'/index/ops/get',
  component:() => import('@/views/ops/getBookRecord.vue')
},

{
  path:'/index/ops/login',
  component:() => import('@/views/ops/loginRecord.vue')
},

{
  path:'/index/ops/message',
  component:() => import('@/views/ops/comments.vue')
},

{
  path:'/index/advice/message',
  component:() => import('@/views/advice/comment.vue')
},

{
  path:'/index/advice/email',
  component:() => import('@/views/advice/email.vue')
},

  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/login.vue')
  },

  {
    path: '*',
    name: '404',
    component: () => import('../views/Home/NotFount.vue')
  }
  
]

const router = new VueRouter({
  routes
})

export default router
