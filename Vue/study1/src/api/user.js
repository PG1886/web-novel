import request from "@/util/request";

export function getUsers(params={}){
    return request({
        method:'GET',
        url:'/user/getall',
        params,
    })
}
