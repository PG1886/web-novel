import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import MyVue from './views/MyVue.vue'
//import g from "./views/Element.vue"
import Element from "element-ui"
import 'element-ui/lib/theme-chalk/index.css'
import api from '@/api'

// 引入echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
Vue.prototype.$api=api
Vue.config.productionTip = false
Vue.use(Element)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
