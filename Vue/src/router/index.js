import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login',
    name: 'home',
    component: Vue,
   
  },
  {
    path: '/index',
    redirect: '/index/home',
    component: () => import("@/views/EnterIndex.vue")
  },
 {
        path: '/login',
        name: 'login',
        component: () => import("@/views/logo/UserLogin.vue")
      },
      {
        path: '*',
        redirect: '404'
      },
  {
    path: '/404',
    component: () => import("@/views/index/NotFound.vue")
  },


  {
    path:'/index/home',
    component: () => import("@/views/index/IndexHome.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/sort',
    component: () => import("@/views/index/SortNovel.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/user',
    component: () => import("@/views/index/UseIndex.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/detail',
    component: () => import("@/views/index/NovelIndex.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/settings',
    component: () => import("@/views/index/UserSettings.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/author',
    component: () => import("@/views/index/AuthorIndex.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },
  {
    path: '/index/write',
    component: () => import("@/views/index/EditNovel.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },

  {
    path:'/index/classify',
    component: () => import("@/views/index/ClassifyNovel.vue")
    ,
    // meta: {
    //   requiresAuth: true
    // }
  },

]

const router = new VueRouter({
  routes
})


router.beforeEach((to, from, next) => {
  // 判断该路由是否需要登录权限
  if (to.meta.requiresAuth) {
    const isAuthenticated = localStorage.getItem('token');
    if (isAuthenticated) {
      next();
    } else {
      // 没有登录，跳转到登录页面
      next({
        path: '/login',
        query: { redirect: to.fullPath } // 将跳转的路由path作为参数，登录成功后跳转到该路由
      });
    }
  } else {
    next();
  }
});

export default router
