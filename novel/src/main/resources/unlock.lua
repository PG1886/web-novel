-- 查询操作获取锁的线程标识
local id = redis.call('get',KEYS[1])
-- 与当前线程标识进行对比
if (id == ARGV[1]) then
    -- 当前线程持有锁，进行删除
    return redis.call('del',KEYS[1])
end
return 0