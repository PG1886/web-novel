function getLoginPhoto() {
    // 获取用户头像
    $.ajax({
        type:'get',
        url:'/user/userinfo',
        success :function(body) {
            if (body.code != 200) {
                alert("用户信息错误，重新登陆.reason : " +  body.message)
                location.assign("login.html");
            } 
            let photo = document.querySelector(".p");
            photo.src = body.data.photo;
        }
    })
}

function makeNovels(data) {
    // 每4个一行
    let count = 0;
    let first = document.querySelector(".hot-novel");
    for (let novel of data) {
        console.log(novel)
        if (count % 4 == 0) {
            let row = document.createElement("div");
            row.className = "row";
        }

       let item = document.createElement("div");
       item.className = "item";
    

       let imgDiv = document.createElement("div");
       imgDiv.className = "img";
       let a = document.createElement("a");
       a.href = "novelIndex.html?id=" + novel.id;
       let img = document.createElement("img");
       img.src = novel.photo;
       a.appendChild(img);
       imgDiv.appendChild(a); 
       item.appendChild(imgDiv)


       let cDiv = document.createElement("div");
       let cA = document.createElement("a")
       cA.href = "novelIndex.html?id=" + novel.id;
       let title = document.createElement("h3");
       title.innerHTML = novel.novelName;
       let desc = document.createElement("div");
       desc.innerHTML = novel.description;
       cA.appendChild(title);
       cDiv.appendChild(cA)
       cDiv.appendChild(desc)
       item.appendChild(cDiv)

       first.appendChild(item);
       count++;

    }
}

// 点击搜索按钮：跳转到主页？key=guanjz
function search() {
    
        // 获取输入信息
        let input = document.querySelector("#key")
        let key = input.value;

        // 判断
        if (key == "") {
            return;
        }

        location.href = "search.html?key=" + key;
    
    
}


function getNovelByType() {
    $.ajax({
        url:'/novel/type' + location.search,
        type: 'get',
        success: function(body) {
            if (body.code != 200) {
                alert("请求失败");
                location.assign = ("index.html");
            }

            makeNovels(body.data)
        }
    }) 
}