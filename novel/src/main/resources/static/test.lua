-- 获取参数
-- 1.商品id
local productId = ARGV[1];
-- 2.用户id
local userId = ARGV[2];

-- 构造缓存的key
-- 1.订单key
local orderKey = "secKill:order" .. productId;
-- 2.库存id
local stockKey = "secKill:stock" .. productId;

-- 判断库存是否足够
if (tonumber(redis.call('get',stockKey)) <= 0) then
    -- 库存不足 返回1
    return 1;
end

-- 判断是否下过单
if (redis.call('sismember',orderKey,userId) == 1) then
    -- 存在 返回2
    return 2;
end

-- 满足扣减库存
redis.call('incrby',stockKey,-1);
-- 下单：缓存订单中加入该用户
redis.call("sadd",orderKey,userId);
return 0;