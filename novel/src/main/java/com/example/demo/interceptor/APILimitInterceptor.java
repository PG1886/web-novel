package com.example.demo.interceptor;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.component.RabbitMQComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 *
 * 接口访问限制拦截器
 */
@Slf4j
public class APILimitInterceptor implements HandlerInterceptor {

    private RedisUtil redisUtil;

    private TokenUtil tokenUtil;

    private RabbitMQComponent rabbitMQComponent;

    public APILimitInterceptor(RedisUtil redisUtil, TokenUtil tokenUtil, RabbitMQComponent rabbitMQComponent) {
        this.redisUtil = redisUtil;
        this.tokenUtil = tokenUtil;
        this.rabbitMQComponent = rabbitMQComponent;
    }

    /**
     * 拦截请求，判断接口访问是否受限
     * @param request HttpServletRequest对象
     * @param response HttpServletResponse对象
     * @param handler 请求处理器对象
     * @return true表示接口访问未受限，false表示接口访问受限
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("OPTIONS")) { // 如果是预检请求
            String headers = request.getHeader("Access-Control-Request-Headers"); // 获取请求头信息
            if (headers != null && headers.contains("token")) { // 检查请求头信息是否包含 Authorization
                response.setHeader("Access-Control-Allow-Headers", "token"); // 在响应中包含 Access-Control-Allow-Headers 头部信息，允许实际请求中包含 Authorization 请求头
                return true;
            }
        }
        // 获取IP
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        log.info("IP:" + ip + " 请求获取限流接口");
        // 使用Redis进行接口访问限制，设置1分钟内访问次数不能超过1次
        return redisUtil.isRequest(RedisUtil.LIMIT_IP_KEY + ip, 1, RedisUtil.LIMIT_IP_TTL, TimeUnit.MINUTES);
    }
}