package com.example.demo.interceptor;

import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.User;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * 对用户登录权限的校验进行统一拦截处理
 */
public class LoginInterceptor implements HandlerInterceptor {

    private TokenUtil tokenUtil;
    private StringRedisTemplate stringRedisTemplate;

    public LoginInterceptor(TokenUtil tokenUtil,StringRedisTemplate stringRedisTemplate) {
        this.tokenUtil = tokenUtil;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 对用户的访问请求进行统一的登录校验
     * @param request
     * @param response
     * @param handler
     * @return false 拦截请求  true 请求继续
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        // 获取用户信息
//        HttpSession session = request.getSession(false);
//        if (session == null || session.getAttribute(ApplicationVariable.USER_SESSION_KRY) == null) {
//            // 用户未登录，进行跳转至登录
//            response.sendRedirect("/login.html");
//            // 返回false拦截改访问请求，先进行登录
//            return false;
//        }
        // 获取请求头的token
//       if (request.getHeader("Access-Control-Request-Headers").equals("token")) {
//           System.out.println( request.getHeader("token"));
//           return true;
//       }

        if (request.getMethod().equals("OPTIONS")) { // 如果是预检请求
            String headers = request.getHeader("Access-Control-Request-Headers"); // 获取请求头信息
            if (headers != null && headers.contains("token")) { // 检查请求头信息是否包含 Authorization
                response.setHeader("Access-Control-Allow-Headers", "token"); // 在响应中包含 Access-Control-Allow-Headers 头部信息，允许实际请求中包含 Authorization 请求头
                return true;
            }
        }
       
        String token = request.getHeader("token");
        if (!StringUtils.hasLength(token)) {
            // 未登录
            return false;
        }
        User user =  tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        // 判断是否存在
        if (user == null) {
            return false;
        }
        // 刷新token有效期
        tokenUtil.setRedisKeyTTL(RedisUtil.TOKE_USER_KEY + token,RedisUtil.TOKEN_USER_TTL, TimeUnit.MINUTES);

        // UV统计用户访问量
        stringRedisTemplate.opsForHyperLogLog().add(RedisUtil.VISIT_COUNT_KEY,token);

        // 已经登录 true 让请求去controller里访问接口
        return true;
    }
}
