package com.example.demo.interceptor;

import com.example.demo.common.ReturnModel;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常格式的处理
 */
@ControllerAdvice
public class ExceptionReturn {
    /**
     * 将异常信息进行返回
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object Exception(Exception e) {
        return ReturnModel.fail(-1,e.getMessage(),null);
    }
}
