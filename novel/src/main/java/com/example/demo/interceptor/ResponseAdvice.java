package com.example.demo.interceptor;

import com.example.demo.common.ReturnModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 统一数据格式的返回，对返回响应的数据格式进行统一处理
 */
@ControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice {
    @Autowired
    private ObjectMapper objectMapper;

    /**
     *
     * @param returnType
     * @param converterType
     * @return true进行处理
     */
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    /**
     * 进行数据格式检测：如果是统一格式就返回，如果是String进行处理，如果不是封装为统一格式后返回
     * @param body
     * @param returnType
     * @param selectedContentType
     * @param selectedConverterType
     * @param request
     * @param response
     * @return
     */
    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (body instanceof ReturnModel) {
            // 是统一格式直接返回
            return body;
        }

        if (body instanceof String) {
            // 是String进行特殊处理
            return objectMapper.writeValueAsString(ReturnModel.success(body));
        }

        // 封装后返回
        return ReturnModel.success(body);
    }
}
