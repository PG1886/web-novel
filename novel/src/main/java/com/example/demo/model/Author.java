package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Author implements Serializable {
    private Integer id;
    private String username;
    private String nickname;
    private String password;
    private Integer empower;
    private Integer uid;
    private Integer fan;
    private String ip;
    private Integer state;
}
