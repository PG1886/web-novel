package com.example.demo.model.vo;

import com.example.demo.model.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserVO extends User implements Serializable {
    public Integer code;
}
