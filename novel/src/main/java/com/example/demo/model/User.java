package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 此类与数据库字段相同:
 */

@Data
public class User implements Serializable {
    private Integer id;
    private String username;
    private String nickname;
    private String password;
    private String photo;
    private int likeSize;
    private String email;
    private Integer follow;
    private Integer state;
    private String ip;
}
