package com.example.demo.model.middlewareVO;

import com.example.demo.model.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserMQ extends User implements Serializable {
    public String message;
}
