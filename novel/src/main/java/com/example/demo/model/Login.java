package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Login implements Serializable {
    private Integer id;
    private String username;
    private Integer identity;
    private String ip;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  // 格式化时间
    private LocalDateTime time;

    public Login(String username, Integer identity, String ip) {
        this.username = username;
        this.identity = identity;
        this.ip = ip;
    }

    public Login() {}
}
