package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Application {
    private Integer id;
    private Integer uid;
    private Integer sex;
    private String nickname;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")  // 格式化时间
    private LocalDateTime startTime;
    private Integer state;
}
