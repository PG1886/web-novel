package com.example.demo.model.vo;

import com.example.demo.model.Remark;
import lombok.Data;

import java.io.Serializable;

@Data
public class RemarkVO extends Remark implements Serializable {
    private String nickname;
    private String photo;
}
