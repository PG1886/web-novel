package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Chapter implements Serializable {
    private Integer id;
    private String title;
    private String content;
    private Integer nid;
    private Integer aid;
    private Integer cid;
}
