package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据库书架表映射类
 */
@Data
public class Love implements Serializable {
    private Integer id;
    private Integer uid;
    private Integer nid;
}
