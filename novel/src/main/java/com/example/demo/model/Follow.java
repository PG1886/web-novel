package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 关注记录表的映射类
 */
@Data
public class Follow implements Serializable {
    private Integer id;
    private Integer uid;
    private Integer aid;
}
