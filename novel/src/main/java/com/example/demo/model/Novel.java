package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Novel implements Serializable {
    private Integer id;
    private String novelName;
    private String pseudonym;
    private Integer aid;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  // 格式化时间
    private LocalDateTime updateTime;
    private Integer readCount;
    private Integer upvote;
    private Integer type;
    private Integer state;
    private String description;
    private String photo;
}
