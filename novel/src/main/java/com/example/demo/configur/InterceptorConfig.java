package com.example.demo.configur;

import com.example.demo.component.RabbitMQComponent;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.interceptor.APILimitInterceptor;
import com.example.demo.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 将拦截器配置入spring
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private RabbitMQComponent rabbitMQComponent;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 添加拦截规则
         */
        registry.addInterceptor(new LoginInterceptor(tokenUtil,stringRedisTemplate))
                .addPathPatterns("/**")                             // 先拦截所有
                .excludePathPatterns("/*.html")                     // 排除前端界面/.excludePathPatterns("/user/login")
                .excludePathPatterns("/user/login")                 // 排除登录接口
                .excludePathPatterns("/user/code")                  // 排除验证码接口
                .excludePathPatterns("/user/enroll");               // 排除注册接口

        registry.addInterceptor(new APILimitInterceptor(redisUtil,tokenUtil,rabbitMQComponent))
                .addPathPatterns("/user/photo")
                .addPathPatterns("/author/write")
                .addPathPatterns("/user/code");                     // 限流接口
    }


    /**
     * 解决前端跨域问题
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)                  // 允许携带cookie
                .allowedHeaders("*") // 允许跨域请求中包含 token 头部信息
                .allowedMethods("*")
                .allowedOrigins("http://localhost:7070");// 跨域地址
    }
}
