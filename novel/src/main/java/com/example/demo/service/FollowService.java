package com.example.demo.service;

import com.example.demo.mapper.FollowMapper;
import com.example.demo.model.Follow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FollowService {

    @Autowired
    private FollowMapper followMapper;

    /**
     * 关注
     * @param follow
     * @return
     */
    public int follow(Follow follow) {
        return followMapper.follow(follow);
    }

    /**
     * 取关
     * @param follow
     * @return
     */
    public int delFollow(Follow follow) {
        return followMapper.delFollow(follow);
    }

    /**
     * 是否关注
     * @param follow
     * @return
     */
    public boolean isFollow(Follow follow) {
        Follow db = followMapper.isFollow(follow);
        if (db != null && db.getAid() == follow.getAid() && db.getUid() == db.getUid()) {
            return true;
        }
        return false;
    }
}
