package com.example.demo.service;

import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.Md5WithSalt;
import com.example.demo.common.SendEmail;
import com.example.demo.component.RabbitMQComponent;
import com.example.demo.component.TokenUtil;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Login;
import com.example.demo.model.User;
import com.example.demo.model.middlewareVO.UserMQ;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Random;

@Slf4j
@Service
public class UserService {

    @Autowired
    private RabbitMQComponent rabbitMQComponent;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserMapper userMapper;

    /**
     * 用户登录
     * @param user
     * @return
     */
    public User login(User user) {
        return userMapper.login(user);
    }

    /**
     * 用户登录记录
     * @param record
     * @return
     */
    public int loginRecord(Login record) {
        return userMapper.loginRecord(record);
    }

    /**
     * 用户组测
     * @param user
     * @return
     */
    public int enroll(User user) {
        return userMapper.enroll(user);
    }

    /**
     * 用户修改信息
     * @param user
     * @return
     */
    public int updateInfo(User user) {
        if (!StringUtils.hasLength(user.getPhoto())) {
            user.setPhoto(null);
        }

        if (!StringUtils.hasLength(user.getNickname())){
            user.setNickname(null);
        }

        if (StringUtils.hasLength(user.getPassword())) {
            user.setPassword(Md5WithSalt.encrypt(user.getPassword()));
        }
        return userMapper.updateInfo(user);
    }

    /**
     * 关注数量自增
     * @param id
     * @return
     */
    public int addFollow(Integer id) {
        return userMapper.addFollow(id);
    }

    /**
     * 关注数量自减
     * @param id
     * @return
     */
    public int delFollow(Integer id) {
        return userMapper.delFollow(id);
    }

    /**
     * 收藏数量自增
     * @param id
     * @return
     */
    public int addLikeSize(Integer id) {
        return userMapper.addLikeSize(id);
    }

    /**
     * 收藏数量自减
     * @param id
     * @return
     */
    public int delLikeSize(Integer id) {
        return userMapper.delLikeSize(id);
    }

    /**
     * 发送邮件
     * @param email
     * @return
     */
    public Integer emailCode(String email) {
        // 生成验证码
        Integer code = new Random().nextInt(9000) + 1000;
        // 发送邮件
        try {
            SendEmail.sendCodeEmail(ApplicationVariable.EMAIL_FROM,email,"Novel", ApplicationVariable.EMAIL_CODE_MESSAGE(code));
            log.info(email + "请求发送验证码邮件：" + code);
            return code;
        } catch (EmailException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将用户信息存入延迟队列
     * @param user
     */
    public void insertMQ(User user) {
        // 获取用户信息
        UserMQ userMQ = new UserMQ();
        userMQ.setEmail(user.getEmail());
        userMQ.setMessage(ApplicationVariable.ENROLL_REMIND);
        // 进入MQ
        rabbitMQComponent.sendLogin(userMQ);
        // 打印日志
        log.info(userMQ.toString() + "注册成功，进入延迟队列发送邮件");
    }
}
