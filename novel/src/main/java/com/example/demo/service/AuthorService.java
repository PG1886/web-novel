package com.example.demo.service;

import com.example.demo.mapper.AuthorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

    @Autowired
    private AuthorMapper authorMapper;

    /**
     * 查询是否是作者
     * @param id
     * @return
     */
    public int isAuthor(Integer id) {
        return authorMapper.isAuthor(id);
    }

    /**
     * 作者粉丝+1
     * @param id
     * @return
     */
    public int increase(Integer id) {
        return authorMapper.increase(id);
    }

    /**
     * 作者粉丝-1
     * @param id
     * @return
     */
    public int decrease(Integer id) {
        return authorMapper.decrease(id);
    }

    /**
     * 查询粉丝个数
     * @param id
     * @return
     */
    public int fan(Integer id) {
        return authorMapper.fan(id);
    }
}
