package com.example.demo.service;

import com.example.demo.mapper.RemarkMapper;
import com.example.demo.model.Remark;
import com.example.demo.model.vo.RemarkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RemarkService {

    @Autowired
    private RemarkMapper remarkMapper;

    /**
     * 根据小说id查询评论
     * @param id
     * @return
     */
    public List<RemarkVO> remarks(Integer id) {
        return remarkMapper.remarks(id);
    }

    /**
     * 添加评论
     * @param remark
     * @return
     */
    public int remarkNovel(Remark remark) {
        return remarkMapper.remarkNovel(remark);
    }
}
