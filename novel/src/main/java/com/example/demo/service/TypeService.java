package com.example.demo.service;

import com.example.demo.mapper.TypeMapper;
import com.example.demo.model.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TypeService {

    @Autowired
    private TypeMapper typeMapper;

    /**
     * 查询类型
     * @return
     */
    public List<Type> types() {
        return typeMapper.types();
    }
}
