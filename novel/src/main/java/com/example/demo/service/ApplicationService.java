package com.example.demo.service;

import com.example.demo.mapper.ApplicationMapper;
import com.example.demo.model.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService {

    @Autowired
    private ApplicationMapper applicationMapper;

    /**
     * 是否添加成功
     * @param application
     * @return
     */
    public boolean add(Application application) {
        return applicationMapper.add(application) == 1;
    }
}
