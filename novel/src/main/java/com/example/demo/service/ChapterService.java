package com.example.demo.service;

import com.example.demo.mapper.ChapterMapper;
import com.example.demo.model.Chapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChapterService {

    @Autowired
    private ChapterMapper chapterMapper;

    /**
     * 查询具体章节信息
     * @param chapter
     * @return
     */
    public Chapter chapter(Chapter chapter) {
        return chapterMapper.chapter(chapter);
    }

    /**
     * 查询小说具体章节的标题
     * @param id
     */
    public List<Chapter> chaptersByNid(Integer id) {
        return chapterMapper.chaptersByNid(id);
    }

    /**
     * 添加章节信息
     * @param chapter
     * @return
     */
    public int add(Chapter chapter) {
        return chapterMapper.add(chapter);
    }
}
