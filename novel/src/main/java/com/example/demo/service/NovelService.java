package com.example.demo.service;

import com.example.demo.mapper.LoveMapper;
import com.example.demo.mapper.NovelMapper;
import com.example.demo.model.Chapter;
import com.example.demo.model.Novel;
import com.example.demo.model.Remark;
import com.example.demo.model.vo.RemarkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NovelService {
    @Autowired
    private NovelMapper novelMapper;

    @Autowired
    private LoveMapper loveMapper;
    /**
     * 根据类型查询对应的小说
     * @param type
     * @return
     */
    public List<Novel> getNovelsByType(Integer type) {
        return novelMapper.getNovelsByType(type);
    }

    /**
     * 根据id查询小说具体信息
     * @param id
     * @return
     */
    public Novel novelDetail(Integer id) {
        return novelMapper.novelDetail(id);
    }

    /**
     * 根据类型查询小说排行
     * @return
     */
    public List<Novel> sortByType() {
        return novelMapper.sortByType();
    }

    /**
     * 根据小说id查询作者id
     * @param id
     * @return
     */
    public Integer queryAidById(Integer id) {
        return novelMapper.queryAidById(id);
    }

    /**
     * 搜索
     * @param keys
     * @return
     */
    public List<Novel> search(List<String> keys) {
        return novelMapper.search(keys);
    }

    /**
     * 添加小说
     * @param novel
     * @return
     */
    public int add(Novel novel) {
        return novelMapper.add(novel);
    }

    /**
     * 点赞+1
     * @param id
     * @return
     */
    public int increase(Integer id) {
        return novelMapper.increase(id);
    }

    /**
     * 点赞-1
     * @param id
     * @return
     */
    public int decrease(Integer id) {
        return novelMapper.decrease(id);
    }

    /**
     * 阅读+1
     * @param id
     * @return
     */
    public int read(Integer id) {
        return novelMapper.read(id);
    }

    /**
     * 查询用户书架
     * @param userId
     * @return
     */
    public List<Novel> bookshelf(Integer userId) {
        // 1.根据用户id查询收藏表
        List<Integer> novelIds = loveMapper.getNidByUid(userId);

        // 2.获取到小说id的集合
        if (novelIds == null || novelIds.isEmpty()) {
            // 没有收藏，则进行处理
            return null;
        }
        List<Novel> novels = novelMapper.getNovelsByIds(novelIds);

        // 3.根据小说id集合查询小说返回
        return novels;
    }

    /**
     * 推荐算法
     * @param userId
     * @return
     */
    public List<Novel> recommend(Integer userId) {
        // 1. 根据用户id查询用户收藏小说id，如果没有则按阅读量进行查询
        // 1.1 拿着小说id查询哪种类型最多，查询该类型80%的小说  20%按照阅读量排行返回
        // 1.2 用户没有收藏，则按照阅读量进行查询返回
        List<Novel> novels = novelMapper.sortByType();

        // 2. 查询小说后返回进行缓存重建
        return novels;
    }

    /**
     * 根据ids查询novels
     * @param ids
     * @return
     */
    public List<Novel> queryNovelByIds(List<Integer> ids) {
        return novelMapper.queryNovelByIds(ids);
    }
}
