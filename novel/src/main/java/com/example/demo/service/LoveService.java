package com.example.demo.service;

import com.example.demo.mapper.LoveMapper;
import com.example.demo.model.Love;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class LoveService {
    @Autowired
    private LoveMapper loveMapper;

    /**
     * 收藏小说
     * @param love
     * @return
     */
    public int love(Love love) {
        return loveMapper.love(love);
    }

    /**
     * 取消收藏
     * @param love
     * @return
     */
    public int delLove(Love love) {
        return loveMapper.delLove(love);
    }

    /**
     * 查询是否收藏
     * @param love
     * @return
     */
    public boolean isLove(Love love) {
        Love db = loveMapper.isLove(love);
        if (db != null && Objects.equals(db.getUid(), love.getUid()) && Objects.equals(db.getNid(), love.getNid())) {
            return true;
        }
        return false;
    }
}
