package com.example.demo.service;

import com.example.demo.mapper.IndexAdMapper;
import com.example.demo.model.AD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexAdService {

    @Autowired
    private IndexAdMapper indexAdMapper;

    /**
     * 查询主页广告图片
     * @return
     */
    public List<AD> ads() {
        return indexAdMapper.ads();
    }
}
