package com.example.demo.controller;

import cn.hutool.core.util.StrUtil;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.model.AD;
import com.example.demo.model.Chapter;
import com.example.demo.service.IndexAdService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.concurrent.TimeUnit;

@ResponseBody
@Controller
@RequestMapping("/ad")
public class IndexAdController {

    @Autowired
    private IndexAdService indexAdService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @GetMapping("/ads")
    public ReturnModel getPhoto() {
        // 1. 查询redis
        String json = stringRedisTemplate.opsForValue().get(RedisUtil.AD_KEY);
        System.out.println(json);
        if (StringUtils.hasLength(json)) {
            List<AD> ads = objectMapper.readValue(json, new TypeReference<List<AD>>() {});
            return ReturnModel.success(200,ads);
        }

        // 2. 没有查询数据库，做缓存重建
        List<AD> ads = indexAdService.ads();
        if (ads == null || ads.isEmpty()) {
            stringRedisTemplate.opsForValue().set(RedisUtil.AD_KEY,"",RedisUtil.TTL, TimeUnit.MINUTES);
        }
        stringRedisTemplate.opsForValue().set(RedisUtil.AD_KEY,objectMapper.writeValueAsString(ads),
                RedisUtil.TTL,TimeUnit.MINUTES);

        // 3. 数据库中没有则使用默认
        System.out.println(ads);
        return ReturnModel.success(200,ads);
    }
}
