package com.example.demo.controller;

import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.model.Chapter;
import com.example.demo.model.Type;
import com.example.demo.service.ChapterService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/chapters")
public class ChapterController {

    @Autowired
    private ChapterService chapterService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 章节详情
     * @param chapter 章节对象
     * @return 根据小说id与章节id进行查容询具体的小说章节具体内
     */
    @GetMapping("/chapter")
    public ReturnModel chapter(Chapter chapter) {
        // 参数校验
        if (chapter == null
                || chapter.getCid() == null
                || chapter.getCid() <= 0
                || chapter.getNid() == null
                || chapter.getNid() <= 0) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 交给服务层
        Chapter dbChapter = chapterService.chapter(chapter);
        // 判断处理结果
        if (dbChapter == null) {
            return ReturnModel.fail(-1,"查询失败");
        }

        // 返回结果
        return ReturnModel.success(200,dbChapter);
    }

    /**
     * 获取小说的章节
     * @param id
     * @return
     */
    @SneakyThrows
    @GetMapping("/list")
    public ReturnModel list(@RequestParam("id") @NotNull @Positive Integer id) {
        // 注解自动校验参数
        // Redis缓存优化性能：请求到达服务器时，先查询缓存，如果缓存没有则查询对应的数据库，如果数据没有则发生缓存穿透，此时使用缓存null解决
        // 1.查询缓存
        String json = stringRedisTemplate.opsForValue().get(RedisUtil.CHAPTERS_KEY + id);
        // 2.判断是否存在
        if (StringUtils.hasLength(json)) {
            // 存在则直接返回
            List<Chapter> chapters = objectMapper.readValue(json,new TypeReference<List<Chapter>>() {});
            return ReturnModel.success(200,chapters);
        }

        // 2.不存在则查询数据库后重建缓存
        List<Chapter> result = chapterService.chaptersByNid(id);
        if (result == null || result.isEmpty()) {
            // 缓存空值
            stringRedisTemplate.opsForValue().set(RedisUtil.CHAPTERS_KEY + id, "",
                    RedisUtil.TTL, TimeUnit.MINUTES);
        }
        stringRedisTemplate.opsForValue().set(RedisUtil.CHAPTERS_KEY + id, objectMapper.writeValueAsString(result),
                RedisUtil.TTL, TimeUnit.MINUTES);
        return ReturnModel.success(200,result);
    }
}
