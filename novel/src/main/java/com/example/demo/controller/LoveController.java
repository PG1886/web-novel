package com.example.demo.controller;

import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.Love;
import com.example.demo.model.User;
import com.example.demo.service.LoveService;
import com.example.demo.service.NovelService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/loves")
@ResponseBody
public class LoveController {
    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private LoveService loveService;

    @Autowired
    private UserService userService;

    @Autowired
    private NovelService novelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


//    未使用redis，使用数据库，请求直接打到数据库
//    /**
//     * 收藏
//     * @param id 小说id
//     * @param request
//     * @return
//     */
//    @PostMapping("/love")
//    @Transactional
//    public ReturnModel love(@RequestParam("id") Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // 构造Love对象
//        Love love = new Love();
//        // 1.小说id
//        love.setNid(id);
//
//        // 2.用户id
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//        Integer userId = user.getId();
//        if (userId == null || userId <= 0) {
//            return ReturnModel.fail(-2,"用户信息不存在");
//        }
//        love.setUid(userId);
//
//        // 服务层处理数据
//        int result = loveService.love(love);
//        if (result == 0) {
//            return ReturnModel.fail(-1,"收藏失败");
//        }
//
//        // 用户表收藏字段自增
//        userService.addLikeSize(userId);
//
//        // 返回
//        return ReturnModel.success(200,"收藏成功",null);
//    }
//
//    /**
//     * 取消收藏
//     * @param id 小说id
//     * @return
//     */
//    @PostMapping("/del")
//    public ReturnModel delLove(@RequestParam("id") Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // 构造Love对象
//        Love love = new Love();
//        // 1.小说id
//        love.setNid(id);
//
//        // 2.用户id
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token, User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//        Integer userId = user.getId();
//        if (userId == null || userId <= 0) {
//            return ReturnModel.fail(-2,"用户信息不存在");
//        }
//        love.setUid(userId);
//
//        // 服务层处理数据
//        int result = loveService.delLove(love);
//        if (result == 0) {
//            return ReturnModel.fail(-1,"取消失败");
//        }
//
//        // 用户收藏字段自减
//        userService.delLikeSize(userId);
//
//        // 返回
//        return ReturnModel.success(200,"取消成功",null);
//    }

//    /**
//     * 查询是否收藏
//     * @param id
//     * @param request
//     * @return
//     */
//    @GetMapping("/isLove")
//    public ReturnModel isLove(@RequestParam("id")Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // 获取用户Id
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//
//        // 获取用户id
//        Integer userId = user.getId();
//        if (userId == null || userId <= 0) {
//            return ReturnModel.fail(-1,"您尚未登录");
//        }
//
//        // 交给服务层
//        Love love = new Love();
//        love.setNid(id);
//        love.setUid(userId);
//        boolean isLove = loveService.isLove(love);
//
//        // 返回结果
//        return isLove ? ReturnModel.success(201,201) : ReturnModel.success(-201,-201);
//    }


   //Redis缓存优化性能
    /**
     * 查询是否喜欢
     * @param id 文章ID，不能为空且必须为正整数
     * @param request HTTP请求对象
     * @return 返回结果对象，如果已喜欢则返回201，未喜欢则返回-201
     */
    @GetMapping("/isLove")
    public ReturnModel isLoveByRedis(@RequestParam("id") @NotNull @Positive Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
        // 根据token获取user
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }
        Integer userId = user.getId();

        // 查询redis是否存在该ID
        Boolean isLove = stringRedisTemplate.opsForSet().isMember(RedisUtil.LOVE_KEY + id,userId.toString());
        // 如果存在，则返回喜欢状态码201；如果不存在，则返回未喜欢状态码-201
        return Boolean.TRUE.equals(isLove) ? ReturnModel.success(201) : ReturnModel.success(-201);
    }

    @GetMapping("/love")
    public ReturnModel loveOrDel(@RequestParam("id") Integer id,@RequestHeader("token")String token) {
        // 1.获取用户id
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }
        Integer userId = user.getId();

        // 2.查询当前用户是否收藏
        Boolean isLove = stringRedisTemplate.opsForSet().isMember(RedisUtil.LOVE_KEY + id, userId.toString());
        // 2.1 构造收藏对象
        Love love = new Love();
        love.setUid(userId);
        love.setNid(id);

        // 3.根据查询结果进行处理
        if (Boolean.TRUE.equals(isLove)) {
            // 3.1.1 已收藏则将数据库中收藏信息进行删除
            int result = loveService.delLove(love);
            // 小说点赞数-1 用户收藏数-1 可以使用MQ异步处理
            userService.delLikeSize(userId);
            novelService.decrease(id);
            // 3.1.2 再将redis中的该小说对应的set集合中的用户id删除
            if (result == 1) {
                stringRedisTemplate.opsForSet().remove(RedisUtil.LOVE_KEY + id,userId.toString());
            }
        } else {
            // 3.2.1 未收藏则在将该收藏信息存储到数据库
            int result = loveService.love(love);
            // 小说点赞数+1 用户收藏数+1
            userService.addLikeSize(userId);
            novelService.increase(id);
            // 3.2.2 然后再将该用户的id作为value，该小说的id作为key组成存入redis中set数据结构
            if (result == 1) {
                stringRedisTemplate.opsForSet().add(RedisUtil.LOVE_KEY + id,userId.toString());
            }
        }


        // 4.返回结果
        return ReturnModel.success(200,"操作成功",null);
    }
}
