package com.example.demo.controller;

import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RabbitMQComponent;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.Application;

import com.example.demo.model.User;
import com.example.demo.model.middlewareVO.UserMQ;
import com.example.demo.service.ApplicationService;
import com.example.demo.service.AuthorService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;

@Controller
@ResponseBody
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private RabbitMQComponent rabbitMQComponent;

    /**
     * 申请成为作者
     * @param application 申请信息
     * @param request 请求对象
     * @return 返回结果
     */
    @GetMapping("/apply")
    public ReturnModel apply(@NotNull Application application,@RequestHeader("token")String token, HttpServletRequest request) {
        // 1.参数校验
        if (application.getSex() == null
                || !StringUtils.hasLength(application.getNickname())) {
            return ReturnModel.fail(-2,"非法参数"); // 参数不合法，返回错误信息
        }
        // 2.获取用户信息
        // 从session中获取当前用户信息
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
//        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);

        // 2.1 是否加速审核
        if (application.getState() != null || application.getState() == 3) {
            // 是加速审核，调用MQ API通知管理员
            UserMQ userMQ = new UserMQ();
            userMQ.setEmail(ApplicationVariable.NOTICE_EMAIL);
            userMQ.setMessage(user.getNickname() + "已提交作者申请，请尽快处理审核");
            rabbitMQComponent.sendNotice(userMQ);
        }

        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"您尚未登录"); // 用户未登录，返回错误信息
        }
        application.setUid(user.getId()); // 将用户id设置为申请信息的uid
        // 3.入库
        boolean isSuccess = applicationService.add(application); // 将申请信息入库，返回是否入库成功
        // 4.返回信息
        return isSuccess ? ReturnModel.success(200) : ReturnModel.fail(500,"您已经提交申请");
    }

    /**
     * 验证该用户是否有作者权限
     * @param request 请求对象
     * @return 返回结果
     */
    @GetMapping("/isAuthor")
    public ReturnModel isAuthor(@RequestHeader("token")String token, HttpServletRequest request) {
        // 获取用户id
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
//        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request); // 从session中获取当前用户信息
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"您尚未登录",-201); // 用户未登录，返回错误信息
        }
        // 查询作者表是否存在该信息
        int count = authorService.isAuthor(user.getId());
        if (count > 0) {
            // 是作者
            int fan = authorService.fan(user.getId());
            return ReturnModel.success(fan,201);
        }

        // 不是作者
        return ReturnModel.success(200,-201);
    }

    /**
     * 上传文件
     * @param file
     * @param token
     * @return
     */
    @RequestMapping("/write")
    public ReturnModel writeNovel(@RequestPart("file")MultipartFile file,@RequestHeader("token")String token) {
        // 1. Check if the uploaded file is a directory
        if (!file.getContentType().equals("application/x-zip-compressed")) {
            return ReturnModel.fail(-1,"非法参数");
        }

        // 2. Get user ID from token
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"您尚未登录");
        }
        Integer userId = user.getId();

        // 3. Save the uploaded folder to D://text
        String folderPath = "D://text//" + userId + "//" + file.getOriginalFilename();
        try {
            File folder = new File(folderPath);
            FileUtils.forceMkdirParent(folder);
            file.transferTo(folder);
            return ReturnModel.success(200);
        } catch (IOException e) {
            e.printStackTrace();
            return ReturnModel.fail(-1,"非法参数");
        }
    }
}