package com.example.demo.controller;

import com.example.demo.common.RedisDistributedLock;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.model.Type;
import com.example.demo.service.TypeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@ResponseBody
@Controller
@RequestMapping("/type")
public class TypeController {

    @Autowired
    private TypeService typeService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @GetMapping("/types")
    public ReturnModel types() {
        // 1.查询缓存
        String json = stringRedisTemplate.opsForValue().get(RedisUtil.TYPES_KEY + 1);
        List<Type> types = new ArrayList<>();
        if (StringUtils.hasLength(json)) {
            types = (List<Type>) objectMapper.readValue(json,  new TypeReference<List<Type>>() {});
        }
        // 2.没命中、进行缓存重建
        if (types == null || types.isEmpty()) {
            // 2.1 加互斥锁
            RedisDistributedLock lock = new RedisDistributedLock("types",stringRedisTemplate);
            boolean isLock = lock.tryLock(20);
            // 2.2 查询数据库重建缓存
            if (isLock) {
                // 2.3 返回重建的缓存数据
                log.info(Thread.currentThread().getName() + "获取到锁尝试缓存重建");
                try {
                    // 2.3.1 查询数据库重建缓存
                    types = typeService.types();
                    stringRedisTemplate.opsForValue().set(RedisUtil.TYPES_KEY + 1,objectMapper.writeValueAsString(types));
                } finally {
                    // 2.3.2 最终释放锁
                    lock.unlock();
                }
            } else {
                // 2.4 其他线程获取分布式锁失败
                log.info(Thread.currentThread().getName() + "未获取到锁");
                // 2.5 从队列中取出重建的缓存数据并返回
                int count = 20;
                while (true) {
                    // 轮询查询缓存是否重建完成，完成则返回数据
                    String re = stringRedisTemplate.opsForValue().get(RedisUtil.TYPES_KEY + 1);
                    if (StringUtils.hasLength(re)) {
                        types = (List<Type>) objectMapper.readValue(json, new TypeReference<List<Type>>() {});
                        break;
                    }

                    // 定义最大轮询次数，避免缓存重建失败，线程持续阻塞
                    if (count == 0) {
                        break;
                    }
                    count--;
                }
            }
        }

        // 3.返回数据
        return ReturnModel.success(types);
    }

}
