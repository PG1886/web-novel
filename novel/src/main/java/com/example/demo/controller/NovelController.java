package com.example.demo.controller;

import com.example.demo.common.ReturnModel;
import com.example.demo.component.Crawler;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.*;
import com.example.demo.service.NovelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@ResponseBody
@Controller
@RequestMapping("/novel")
public class NovelController {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NovelService novelService;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Crawler crawler;

    /**
     * 分类
     * API : get novels by type
     * @param type
     * @return
     */
    @GetMapping("/type")
    public ReturnModel getNovelsByType(@RequestParam("type")Integer type) {
        // 参数校验
        if (type == null || type <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }

        // 交给服务层处理
        List<Novel> list = novelService.getNovelsByType(type);
        if (list == null) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 返回数据
        return ReturnModel.success(200,list);
    }

    /**
     * 主页
     * 获取推荐小说
     * @return0
     */
    @GetMapping("/hot")
    public ReturnModel getHotNovels(@RequestHeader("token")String token) throws JsonProcessingException {
        // 根据用户喜欢进行推荐
        // 1. 获取用户信息
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-2,"您尚未登录");
        }
        Integer userId = user.getId();

        // 2. 查询缓存数据
        Set<String> members = stringRedisTemplate.opsForSet().members(RedisUtil.HOT_USER_KRY + userId);
        if (members != null && !members.isEmpty()) {
            // 1. 根据小说id查询数据库
            List<Integer> ids = members.stream()
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            List<Novel> novels = novelService.queryNovelByIds(ids);
            if (novels == null || novels.isEmpty()) {
                return ReturnModel.success(null);
            }

            // 2. 返回数据
            return ReturnModel.success(novels);
        }

        // 3. 缓存重建推荐算法。推荐小说
        List<Novel> hots = novelService.recommend(userId);
        // 3.2 缓存重建
        String[] ids = new String[Math.min(hots.size(), 10)];
        for (int i = 0; i < hots.size() && i < 10; i++) {
            ids[i] = hots.get(i).getId().toString();
        }
        List<String> elements = Arrays.asList(ids);
        stringRedisTemplate.opsForSet().add(RedisUtil.HOT_USER_KRY + userId, elements.toArray(new String[0]));

        // 4. 返回数据
        return ReturnModel.success(200,hots);
    }

    /**
     * 小说详情
     * @param id
     * @return 根据小说id去查询具体的小说信息返回
     */
    @GetMapping("/detail")
    public ReturnModel novelDetail(@RequestParam("id") Integer id) {
        // 参数校验
        if (id == null || id <= 0) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 交给服务层处理
        Novel novel = novelService.novelDetail(id);
        if (novel == null) {
            return ReturnModel.fail(-1,"查询失败");
        }

        // 阅读量++
        novelService.read(id);

        // 返回结果
        return ReturnModel.success(200,novel);
    }


    /**
     * 排行榜
     * 根据类型查询相关排行榜前10
     * @return
     */
    @GetMapping("/sort")
    public ReturnModel sortByType() {
        // 交给服务层处理
        List<Novel> novels = novelService.sortByType();
        if (novels == null) {
            return ReturnModel.fail(-1,"查询失败");
        }

        // 返回数据
        return ReturnModel.success(200,novels);
    }


    /**
     * 搜索
     * @param key 关键字
     * @return 符合的小说集合
     */
    @GetMapping("/search")
    public ReturnModel search(@RequestParam("key") String key) {
        // 参数校验
        if (!StringUtils.hasLength(key)) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 处理key生成keys
        List<String> keys = new ArrayList<>();
        keys.add("%" + key + "%");
        keys.add(key + "%");
        keys.add("%" + key);

        // 交给服务层
        List<Novel> result = novelService.search(keys);
        if (result == null || result.size() <= 0) {
            return ReturnModel.fail(-3,"暂无数据");
        }

        // 返回结果
        return ReturnModel.success(200,result);
    }

    /**
     * 书架
     * @param token
     * @return 如果返回 code=5，则前端展示您还未收藏书籍
     */
    @GetMapping("/bookshelf")
    public ReturnModel bookshelf(@RequestHeader("token")String token) {
        // 1. 获取用户信息
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }
        Integer userId = user.getId();

        // 2.查询数据库中的收藏数据
        List<Novel> bookshelf = novelService.bookshelf(userId);

        // 3. 返回给前端
        if (bookshelf == null || bookshelf.isEmpty()) {
            return ReturnModel.fail(5,"您还没有收藏");
        }
        return ReturnModel.success(200,bookshelf);
    }
}


