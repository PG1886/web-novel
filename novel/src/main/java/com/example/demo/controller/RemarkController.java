package com.example.demo.controller;

import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.Remark;
import com.example.demo.model.User;
import com.example.demo.model.vo.RemarkVO;
import com.example.demo.service.RemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@ResponseBody
@Controller
@RequestMapping("/remarks")
public class RemarkController {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private RemarkService remarkService;

    /**
     * 查看评论
     * 小说评论信息
     * @param id
     * @return 根据小说id去进行联表查询 返回评论时间，评论内容，评论用户
     */
    @GetMapping("/remarkList")
    public ReturnModel remarks(@RequestParam("id") Integer id) {
        // 参数校验
        if (id == null || id <= 0) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 交给服务层查询
        List<RemarkVO> remarks = remarkService.remarks(id);

        // 判断服务层返回的结果
        if (remarks == null) {
            return ReturnModel.fail(-1,"查询失败");
        }

        // 返回信息
        return ReturnModel.success(200,remarks);
    }

    /**
     * 发表评论
     * @param remark
     * @return
     */
    @GetMapping("/add")
    public ReturnModel remarkNovel(Remark remark, @RequestHeader("token")String token, HttpServletRequest request) {
        // 参数校验
        if (remark == null
                || !StringUtils.hasLength(remark.getContent())
                || remark.getNid() == null
                || remark.getNid() <= 0) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 获取用户信息
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
//        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);

        // 设置用户信息给请求中的remark对象
        Integer userId = user.getId();
        if (userId == null || userId <= 0) {
            return ReturnModel.fail(-1,"您尚未登录");
        }
        remark.setUid(userId);

        // 交给服务层处理
        int result = remarkService.remarkNovel(remark);
        if (result == 0) {
            return ReturnModel.fail(-1,"评论失败");
        }

        // 返回评论成功
        return ReturnModel.success(200,"评论成功",null);
    }
}
