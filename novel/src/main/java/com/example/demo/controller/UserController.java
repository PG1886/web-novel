package com.example.demo.controller;

import com.example.demo.common.*;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.Login;
import com.example.demo.model.User;
import com.example.demo.model.vo.UserVO;
import com.example.demo.service.UserService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Controller
@RequestMapping("/user")
@ResponseBody
public class UserController {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * login API
     * @param user
     * @param request
     * @return
     */
    @SneakyThrows
    @PostMapping("/login")
    public ReturnModel login(@RequestBody User user, HttpServletRequest request) {
        if (user == null
                || !StringUtils.hasLength(user.getUsername())
                || !StringUtils.hasLength(user.getPassword())) {
            // 非法参数
            return ReturnModel.fail(-1,"非法参数");
        }

        // 处理登录请求：查询数据库密码通过MD5封装的方法进行对比
        // 1.查询数据库
        User dbUser = userService.login(user);
        if (dbUser == null || dbUser.getId() <= 0) {
            return ReturnModel.fail(-2,"用户名或密码错误");
        }

        // 2.调用MD5工具类方法进行对比
        if (!Md5WithSalt.check(user.getPassword(),dbUser.getPassword())) {
            // 密码不一样，登录失败
            return ReturnModel.fail(-2,"用户名或密码错误");
        }

        // 3.登录成功，可存入redis key-token:生成的随机唯一值  value：user
        String ip = request.getRemoteAddr();
        dbUser.setPassword("******");
        dbUser.setIp(ip);
        //TokenUtil.setObjectInSession(ApplicationVariable.USER_SESSION_KRY,dbUser,request);

        // 3.redis实现token
        // 3.1 生成随机token
        String token = UUID.randomUUID().toString().replace("-","");
        // 3.2 保存到redis
        String key = RedisUtil.TOKE_USER_KEY + token;
        tokenUtil.setObjectInRedis(key,dbUser,30L, TimeUnit.MINUTES);

        // 4.存入登录记录
        Login record = new Login(dbUser.getUsername(),1,ip);
        int result = userService.loginRecord(record);

        // 5.返回登录成功 返回token  后续携带token
        return ReturnModel.success(200,"登录成功",token);
        // return ReturnModel.success(200,"登录成功","token");
    }

    /**
     * 获取验证码
     * @param email
     * @param request
     * @return
     */
    @GetMapping("/code")
    public ReturnModel emailCode(String email, HttpServletRequest request) {
        // 校验参数
        if (!SendEmail.isEmail(email)) {
            return ReturnModel.fail(-2, "非法参数"); // 如果邮箱格式不合法，返回非法参数
        }

        //  redis实现接口限流
        // 交给服务层
        Integer code = userService.emailCode(email); // 生成验证码
        if (code == null || code <= 0) {
            return ReturnModel.fail(-1, "请求失败请稍后再试"); // 如果生成验证码失败，返回请求失败信息
        }

        // 存入session
        String key = RedisUtil.EMAIL_CODE_KEY + email.replace("@qq.com","");
        tokenUtil.setObjectInRedis(key,code,RedisUtil.EMAIL_CODE_TTL,TimeUnit.MINUTES); // 验证码存入Redis，600秒过期
        // TokenUtil.setObjectInSession(ApplicationVariable.EMAIL_CODE,code,request); // 将验证码存入session中，方便后面的验证

        // 返回成功信息
        return ReturnModel.success(200);
    }
    /**
     * enroll API
     * @param user
     * @return
     */
    @Transactional
    @PostMapping("/enroll")
    public ReturnModel enroll(@RequestBody UserVO user, HttpServletRequest request) {
        // 非法校验
        if (user == null
                || !StringUtils.hasLength(user.getUsername())
                || !StringUtils.hasLength(user.getPassword())
                || !StringUtils.hasLength(user.getEmail())
                || user.getCode() == null) {
            return ReturnModel.fail(-2,"请完善注册信息");
        }

        // 获取生成的验证码与用户输入验证码进行对比
        String key = RedisUtil.EMAIL_CODE_KEY + user.getEmail().replace("@qq.com","");
        Integer code = tokenUtil.getObjectByRedis(key, Integer.class);
//        Integer code = (Integer) TokenUtil.getObjectBySession(ApplicationVariable.EMAIL_CODE,request);
        log.info("用户输入验证码：" + user.getCode() +",正确验证码：" + code);
        if (code == null || !code.equals(user.getCode())) {
            return ReturnModel.fail(-11,"验证码错误");
        }

        // 调用MD5工具类进行密码加密
        String password = user.getPassword();
        password = Md5WithSalt.encrypt(password);
        user.setPassword(password);

        // 获取ip
        String ip = request.getRemoteAddr();
        user.setIp(ip);

        // 注册
        int result = userService.enroll(user);
        if (result != 1) {
            // 注册失败
            return ReturnModel.fail(-2,"请重新输入个人信息");
        }

        // 删除存储的code
        tokenUtil.removeObjectInRedis(RedisUtil.EMAIL_CODE_KEY + user.getEmail().replace("@qq.com",""));
//        TokenUtil.removeObjectInSession(ApplicationVariable.EMAIL_CODE,request);

        // 进入MQ
        userService.insertMQ(user);

        // 返回
        return ReturnModel.success(200,"注册成功",null);
    }

    @GetMapping("userPhoto")
    public ReturnModel getPhoto(@RequestHeader("token")String token) {
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null) {
            return ReturnModel.fail(-2,"您尚未登录");
        }
        return ReturnModel.success(200,user);
    }

    /**
     * 获取个人信息
     * @param request
     * @return
     */
    @GetMapping("/userinfo")
    public ReturnModel getUserInfo(@RequestHeader("token")String token, HttpServletRequest request) {
        // 获取用户信息
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
//        User user = (User) TokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
        if (user == null) {
            return ReturnModel.fail(-2,"您尚未登录");
        }

        // 查询数据库
        User dbUser = userService.login(user);
        dbUser.setPassword("****");
        dbUser.setIp("****");
        dbUser.setEmail("****");

        // 获取数据进行返回
        return ReturnModel.success(200,dbUser);
    }

    /**
     * 上传头像返回头像路径
     * @param file
     * @return
     */
    @RequestMapping("/photo")
    @SneakyThrows
    public ReturnModel photo(@RequestPart(value = "file") MultipartFile file) {
        if (file == null || file.isEmpty()) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 构建路径
        String path = UUID.randomUUID().toString().replace("-", "");

        // 文件加后缀
        path += file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

        // 存入文件
        file.transferTo(new File(ApplicationVariable.PHOTO_ROOT_PATH + path));
        log.info("用户修改头像：" + path);

        // 返回数据
        return ReturnModel.success(path);
    }

    /**
     * 个人设置用户修改信息
     * @param user
     * @return
     */
    @GetMapping("/update")
    public ReturnModel updateInfo(User user, @RequestHeader("token") String token, HttpServletRequest request) throws IOException {
        // 参数校验
        if (user == null || !StringUtils.hasLength(user.getEmail())
                || !StringUtils.hasLength(user.getPassword())
                || !StringUtils.hasLength(user.getNickname())) {
            return ReturnModel.fail(-2,"非法参数");
        }

        // 获取用户id
        User userSession = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
//        User userSession = (User) TokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
        if (userSession.getId() == null || userSession.getId() <= 0) {
            return ReturnModel.fail(-2,"修改失败");
        }
        user.setId(userSession.getId());

        // 交给服务层去处理
        int result = userService.updateInfo(user);
        if (result != 1) {
            return ReturnModel.fail(-2,"修改失败");
        }

        // 此时修改成功：1.删除用户token触发重登，2.修改用户token不用重登
        // 删除session
        tokenUtil.removeObjectInRedis(RedisUtil.TOKE_USER_KEY + token);
//        TokenUtil.removeObjectInSession(ApplicationVariable.USER_SESSION_KRY,request);

        return ReturnModel.success(200,"修改成功",null);
    }

    /**
     * 退出登录，删除redis中的token信息
     * @param token
     * @return
     */
    @GetMapping("/logout")
    public ReturnModel logout(@RequestHeader("token")String token) {
        // 删除token
        Boolean isDelete = stringRedisTemplate.delete(RedisUtil.TOKE_USER_KEY + token);
        return Boolean.TRUE.equals(isDelete) ? ReturnModel.success(200) : ReturnModel.fail(-1,"非法参数");
    }
}
