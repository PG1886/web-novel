package com.example.demo.controller;


import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.ReturnModel;
import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.Follow;
import com.example.demo.model.User;
import com.example.demo.service.AuthorService;
import com.example.demo.service.FollowService;
import com.example.demo.service.NovelService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Controller
@ResponseBody
@RequestMapping("/follows")
public class FollowController {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private NovelService novelService;

    @Autowired
    private FollowService followService;

    @Autowired
    private AuthorService authorService;

//    /**
//     * 关注作者
//     * 添加关注表后，修改用户follow字段
//     * 根据小说id查询作者信息，构造Follow对象，写入follow表，修改userinfo表follow字段
//     * @param id 小说id
//     * @return 成功与否
//     */
//    @PostMapping("/follow")
//    @Transactional
//    public ReturnModel follow(@RequestParam("id") Integer id, @RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // 获取用户信息
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//
//        // 设置用户信息
//        if (user == null || user.getId() == null || user.getId() <= 0) {
//            return ReturnModel.fail(-1,"您尚未登录");
//        }
//        Follow follow = new Follow();
//        follow.setUid(user.getId());
//
//        // 根据小说id查询作者id
//        int aid = novelService.queryAidById(id);
//
//        // 继续构造follow
//        follow.setAid(aid);
//
//        // 操作用户表自增follow 以及 写入follow表
//        userService.addFollow(user.getId());
//        int result = followService.follow(follow);
//        if (result == 0) {
//            return ReturnModel.fail(-1,"关注失败");
//        }
//
//        // 返回结果
//        return ReturnModel.success(200,"关注成功",null);
//    }
//
//    /**
//     * 取关
//     * @param id 小说id
//     * @return 参数校验-》根据小说id查询作者aid-》session获取uid-》构造Follow-》操作follow进行删除-》操作userinfo进行follow自减
//     */
//    @PostMapping("/del")
//    @Transactional
//    public ReturnModel delFollow(@RequestParam("id") Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // session获取uid
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//
//        // 设置uid
//        if (user == null || user.getId() == null || user.getId() <= 0) {
//            return ReturnModel.fail(-1,"您尚未登录");
//        }
//        Follow follow = new Follow();
//        follow.setUid(user.getId());
//
//        // 根据小说id查询作者id
//        int aid = novelService.queryAidById(id);
//        follow.setAid(aid);
//
//        // 操作userinfo进行follow自减 and 操作follow进行删除
//        userService.delFollow(user.getId());
//        int result = followService.delFollow(follow);
//        if (result == 0) {
//            return ReturnModel.fail(-1,"取关失败");
//        }
//
//        // 返回结果
//        return ReturnModel.success(200,"取关成功",null);
//    }
//
//    /**
//     * 查询是否关注
//     * @param id
//     * @param request
//     * @return
//     */
//    @GetMapping("/isFollow")
//    public ReturnModel isFollow(@RequestParam("id") Integer id, @RequestHeader("token")String token, HttpServletRequest request) {
//        // 参数校验
//        if (id == null || id <= 0) {
//            return ReturnModel.fail(-2,"非法参数");
//        }
//
//        // 获取用户Id
//        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
////        User user = (User) tokenUtil.getObjectBySession(ApplicationVariable.USER_SESSION_KRY,request);
//        if (user == null || user.getId() == null || user.getId() <= 0) {
//            return ReturnModel.fail(-1,"您尚未登录");
//        }
//
//        // 获取用户id
//        Integer userId = user.getId();
//
//        // 构造对象
//        Follow follow = new Follow();
//        follow.setUid(userId);
//
//        // 查询aid
//        int aid = novelService.queryAidById(id);
//        if (aid <= 0) {
//            return ReturnModel.fail(-1,"查询失败");
//        }
//        follow.setAid(aid);
//
//        // 查询是否存在
//        boolean isFollow = followService.isFollow(follow);
//
//        // 返回
//        return isFollow ? ReturnModel.success(201,201) : ReturnModel.success(-201,-201);
//    }


    // Redis缓存优化性能
    /**
     * 关注和取消点赞接口（使用Redis缓存防止重复操作）
     * @param id 文章ID，不能为空且必须为正整数
     * @param request HTTP请求对象
     * @return 返回结果对象
     */
    @GetMapping("/follow")
    public ReturnModel followByRedis(@NotNull @Positive Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
        // 1.首先获取用户id
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }
        Integer userId = user.getId();

        // 1.1 构建Follow对象
        Follow follow = new Follow();
        Integer aid = novelService.queryAidById(id);
        follow.setAid(aid);
        follow.setUid(userId);

        // 2.查询当前用户是否关注
        Boolean isFollow = stringRedisTemplate.opsForSet().isMember(RedisUtil.FOLLOW_KEY + aid,userId.toString());
        if (Boolean.TRUE.equals(isFollow)) {
            // 2.1 用户已关注，则将该关注信息从follow表中删除
            int result = followService.delFollow(follow);
            // 用户表关注字段-1 作者粉丝数-1 可以使用MQ异步处理
            userService.delFollow(userId);
            authorService.decrease(aid);
            // 2.2 从redis中移除
            if (result == 1) {
                stringRedisTemplate.opsForSet().remove(RedisUtil.FOLLOW_KEY + aid,userId.toString());
            }
        } else {
            // 2.1 用户未关注，则将关注信息写follow表
            int result = followService.follow(follow);
            //  用户表关注字段+1 作者粉丝数+1 可以使用MQ异步处理
            userService.addFollow(userId);
            authorService.increase(aid);
            // 2.2 从redis中添加该用户id
            if (result == 1) {
                stringRedisTemplate.opsForSet().add(RedisUtil.FOLLOW_KEY + aid, userId.toString());
            }
        }

        // 3.返回结果
        return ReturnModel.success(200);
    }

    /**
     * 查询是否关注
     * @param id 文章ID，不能为空且必须为正整数
     * @param request HTTP请求对象
     * @return 返回结果对象，如果已关注则返回201，未关注则返回-201
     */
    @GetMapping("/isFollow")
    public ReturnModel isFollowByRedis(@RequestParam("id") @NotNull @Positive Integer id,@RequestHeader("token")String token, HttpServletRequest request) {
        // 1.首先获取用户id
        User user = tokenUtil.getObjectByRedis(RedisUtil.TOKE_USER_KEY + token,User.class);
        if (user == null || user.getId() == null || user.getId() <= 0) {
            return ReturnModel.fail(-1,"非法参数");
        }
        Integer userId = user.getId();

        // 2.查询redis中是否存在该用户的id
        Integer aid = novelService.queryAidById(id);
        Boolean isFollow = stringRedisTemplate.opsForSet().isMember(RedisUtil.FOLLOW_KEY + aid,userId.toString());

        // 如果存在，则返回关注状态码201；如果不存在，则返回未关注状态码-201
        return Boolean.TRUE.equals(isFollow) ? ReturnModel.success(201) : ReturnModel.success(-201);
    }
}
