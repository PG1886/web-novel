package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class NovelApplication {
	// 获取全局单一对象时，通过原始的spring方式进行获取context.getBean("id")
	private static ApplicationContext context;

	/**
	 * 项目启动入口
	 * @param args
	 */
	public static void main(String[] args) {
		context = SpringApplication.run(NovelApplication.class, args);
	}
}
