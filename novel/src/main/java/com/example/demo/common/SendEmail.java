package com.example.demo.common;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 * 发送邮件工具类，提供发送邮件和判断是否为QQ邮箱的方法。
 */
public class SendEmail {
    /**
     * 发送邮件的方法
     *
     * @param fromEmail 发件人邮箱
     * @param toEmail   收件人邮箱
     * @param subject   邮件主题
     * @param message   邮件内容
     * @throws EmailException 如果邮件发送失败
     */
    public static void sendCodeEmail(String fromEmail, String toEmail,
                                     String subject, String message) throws EmailException {
        SimpleEmail email = new SimpleEmail();
        // 将邮件服务器的端口号设为465（qq邮箱的SMTPS协议端口号）
        email.setSslSmtpPort("465");
        email.setHostName("smtp.qq.com");
        // 设置发件人邮箱和授权码进行身份验证
        email.setAuthentication(fromEmail, "tnnoquadouczeaij");
        email.setCharset("UTF-8");
        try {
            email.setFrom(fromEmail, "NOVEL");
            email.addTo(toEmail);
            email.setSubject(subject);
            email.setMsg(message);
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
            // 发送邮件失败时，抛出异常
            throw e;
        }
    }

    /**
     * 判断是否为邮箱
     *
     * @param email 待判断的邮箱地址
     * @return 如果是邮箱，返回true；否则，返回false。
     */
    public static boolean isEmail(String email) {
        // 邮箱正则表达式
        String regex = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
        return email.matches(regex);
    }
}