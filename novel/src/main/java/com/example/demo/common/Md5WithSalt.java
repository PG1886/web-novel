package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * MD5加盐算法存储密码的实现类
 *
 * 加盐思路： 1.通过UUID拿到盐值  2.拼接：盐值+明文密码  3.对2中数据进行md5加密  4.拼接：  盐值 + ”^“ + 3中的字符串
 * 明文密码走完上述流程后后存入数据库
 * 登录验证： 1.根据用户名在数据库里得到对应的密文  2.根据密文密码获取该用户的盐值  3.将用户输入的登录密码走完上述加盐思路 4.与数据库密文密码进行对比
 */
public class Md5WithSalt {
    /**
     * 用户注册
     * 调用该方法进行md5加盐处理后存入数据库
     * @param password
     * @return
     */
    public static String encrypt(String password) {
        // 1.生成盐值
        String salt = UUID.randomUUID().toString().replace("-","");

        // 2.生成加盐后的md5密码  --- 密码加盐后通过spring内部的md5加密api进行加密
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());

        // 3.生成最终存入数据库的加密密码   65位固定规则
        String finalPassword = salt + "^" + saltPassword;
        return finalPassword;
    }

    /**
     * 用户登录的工具方法
     * 该方法是对用户输入的密码进行上述加盐操作后与数据库密文密码进行比较的工具方法
     * @param password
     * @param salt
     * @return
     */
    public static String encrypt(String password,String salt) {
        // 1.得到加盐之后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());

        // 2.生成最终密码
        String finalPassword = salt + "^" + saltPassword;

        return finalPassword;
    }

    /**
     * 用户登录密码正确性验证
     * @param password           用户登录时输入的密码
     * @param dbFinalPassword    用户数据库里最终的加密密码
     * @return
     */
    public static boolean check(String password,String dbFinalPassword) {
        if (StringUtils.hasLength(password)
                && StringUtils.hasLength(dbFinalPassword)
                && dbFinalPassword.length() == 65) {
            String salt = dbFinalPassword.split("\\^")[0];
            String confirmFinalPassword = encrypt(password,salt);

            if (dbFinalPassword.equals(confirmFinalPassword)) {
                return true;
            }
        }

        return false;
    }
}



