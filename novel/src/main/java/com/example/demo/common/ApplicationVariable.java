package com.example.demo.common;

/**
 * 存放全局变量的类
 */
public class ApplicationVariable {
    // Session 中存放用户信息的 key
    public static final String USER_SESSION_KRY = "USER_SESSION_KRY";
    // 存放照片目录的根路径
    public static final String PHOTO_ROOT_PATH = "D:\\JavaDemo\\Vue\\novel\\src\\assets\\";
    // 存放小说目录的根路径
    public static final String CRAWLER_ROOT_PATH = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\novel\\";
    // 用于存放邮箱验证码的 key
    public static final String EMAIL_SESSION_CODE = "EMAIL_CODE";
    // 主邮箱
    public static final String EMAIL_FROM = "2194819036@qq.com";
    // 通知邮箱
    public static final String NOTICE_EMAIL = "3115597146@qq.com";
    // 用户注册2天后提示登录
    public static final String ENROLL_REMIND = "您已注册novel网站2天，更多精彩小说等您来读";
    /**
     * 根据验证码生成邮件内容
     * @param code 验证码
     * @return 邮件内容
     */
    public static String EMAIL_CODE_MESSAGE(Integer code) {
        return  "尊敬的用户您好：\n"
                + "你的 NOVEL 账户邮箱验证码（" + code + "）有效时间为 5 分钟。\n"
                + "请勿将此验证码转发给他人。如果不是您本人操作，请忽略此邮件。\n"
                + "祝您使用愉快！\n"
                + "\n"
                + "Dear user:\n"
                + "Your NOVEL account email verification code (" + code + ") is valid for 5 minutes.\n"
                + "Please do not forward this verification code to others. If this is not your operation, please ignore this email.\n"
                + "Wish you a happy use!\n";
    }
}