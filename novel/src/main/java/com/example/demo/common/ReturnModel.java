package com.example.demo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据的返回格式模板类
 */
@Data
public class ReturnModel implements Serializable { // 实现支持可序列化的空接口
    private Integer code;       // 状态码
    private String message;     // 状态码描述信息
    private Object data;        // 返回的数据

    /**
     * 要返回响应成功后的数据的格式统一设置
     */
    public static ReturnModel success(Object data) {
        ReturnModel resultModel = new ReturnModel();
        resultModel.setCode(200);
        resultModel.setMessage("");
        resultModel.setData(data);
        return resultModel;
    }

    public static ReturnModel success(int code, Object data) {
        ReturnModel resultModel = new ReturnModel();
        resultModel.setCode(code);
        resultModel.setMessage("");
        resultModel.setData(data);
        return resultModel;
    }

    public static ReturnModel success(int code, String message, Object data) {
        ReturnModel resultModel = new ReturnModel();
        resultModel.setCode(code);
        resultModel.setMessage(message);
        resultModel.setData(data);
        return resultModel;
    }

    /**
     * 要返回响应失败后的数据格式的统一
     */
    public static ReturnModel fail(int code, String message) {
        ReturnModel resultModel = new ReturnModel();
        resultModel.setCode(code);
        resultModel.setMessage(message);
        resultModel.setData(null);
        return resultModel;
    }

    public static ReturnModel fail(int code, String message, Object data) {
        ReturnModel resultModel = new ReturnModel();
        resultModel.setCode(code);
        resultModel.setMessage(message);
        resultModel.setData(data);
        return resultModel;
    }
}

