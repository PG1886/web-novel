package com.example.demo.common;

import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 基于redis实现分布式锁
 */
public class RedisDistributedLock {
    private String name;                              // 需要加锁的业务key
    private StringRedisTemplate stringRedisTemplate; // 操作redis

    private static final String LOCK_KEY_PREFIX = "lock:"; // 加锁key的前缀
    private static final String LOCK_ID_PREFIX = UUID.randomUUID().toString() + "-"; // 区别不同线程的value前缀
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT; // 加载lua脚本
    static {
        // 类加载时就初始化
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua")); // 指定录取去加载
        UNLOCK_SCRIPT.setResultType(Long.class);                        // 返回值类型
    }


    // 提供构造方法
    public RedisDistributedLock(String name, StringRedisTemplate stringRedisTemplate) {
        this.name = name;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    /**
     * 加锁方法
     * @param time  过期时间
     * @return  加锁是否成功
     */
    public boolean tryLock(long time) {
        // 获取线程ID
        String threadId = LOCK_ID_PREFIX + Thread.currentThread().getId();

        // 获取锁
        Boolean isLock = stringRedisTemplate.opsForValue()
                .setIfAbsent(LOCK_KEY_PREFIX + name,threadId,time, TimeUnit.SECONDS);

        // 返回结果   此处需要注意如果直接返回boolean类型会产生拆箱，可能会发送异常，所以使用以下操作
        return Boolean.TRUE.equals(isLock);
    }

    /**
     * 释放锁
     * 将key删除即可
     */
    public void unlock() {
        // 构建keys
        List<String> keys = new ArrayList<>();
        keys.add(LOCK_KEY_PREFIX + name);

        // 执行lua脚本
        stringRedisTemplate.execute(UNLOCK_SCRIPT,keys,LOCK_ID_PREFIX + Thread.currentThread().getId());
    }
}
