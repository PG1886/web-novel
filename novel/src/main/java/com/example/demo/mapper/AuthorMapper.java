package com.example.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AuthorMapper {
    /**
     * 查询作者表是否存在
     * @param uid
     * @return
     */
    int isAuthor(@Param("uid") Integer uid);

    /**
     * 粉丝+1
     * @param id
     * @return
     */
    int increase(@Param("id") Integer id);

    /**
     * 粉丝-1
     * @param id
     * @return
     */
    int decrease(@Param("id") Integer id);

    /**
     * 粉丝个数
     * @param uid
     * @return
     */
    int fan(@Param("uid") Integer uid);
}
