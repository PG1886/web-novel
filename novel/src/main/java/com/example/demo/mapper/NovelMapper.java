package com.example.demo.mapper;

import com.example.demo.model.Chapter;
import com.example.demo.model.Novel;
import com.example.demo.model.Remark;
import com.example.demo.model.vo.RemarkVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NovelMapper {
    /**
     * search novels by type
     * @param type
     * @return
     */
    List<Novel> getNovelsByType(@Param("type") Integer type);


    /**
     * 查询章节详情
     * @param id
     * @return
     */
    Novel novelDetail(@Param("id") Integer id);

    /**
     * 根据类型查询小说：排行榜
     * @return
     */
    List<Novel> sortByType();

    /**
     * 根据小说id查询作者id
     * @param id
     * @return
     */
    Integer queryAidById(@Param("id") Integer id);

    /**
     * 小说搜索
     * @param keys
     * @return
     */
    List<Novel> search(List<String> keys);

    /**
     * 添加小说
     * @param novel
     * @return
     */
    int add(Novel novel);

    /**
     * 小说点赞+1
     * @param id
     * @return
     */
    int increase(@Param("id") Integer id);

    /**
     * 小说点赞-1
     * @param id
     * @return
     */
    int decrease(@Param("id") Integer id);

    /**
     * 小说阅读+1
     * @param id
     * @return
     */
    int read(@Param("id") Integer id);

    /**
     * 根据id查询小说
     * @param novelIds
     * @return
     */
    List<Novel> getNovelsByIds(List<Integer> novelIds);

    /**
     * 根据ids查询novels
     * @param ids
     * @return
     */
    List<Novel> queryNovelByIds(List<Integer> ids);
}
