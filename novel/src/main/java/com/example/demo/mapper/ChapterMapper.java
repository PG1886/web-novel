package com.example.demo.mapper;

import com.example.demo.model.Chapter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChapterMapper {

    /**
     * 查询章节信息
     * @param chapter
     * @return
     */
    Chapter chapter(Chapter chapter);

    /**
     * 根据小说id查询具体章节信息：查询size章即可
     * @param id 小说id
     * @return
     */
    List<Chapter> chaptersByNid(@Param("id") Integer id);

    /**
     * 添加章节
     * @param chapter
     * @return
     */
    int add(Chapter chapter);
}
