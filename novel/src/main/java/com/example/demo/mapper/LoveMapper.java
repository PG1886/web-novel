package com.example.demo.mapper;

import com.example.demo.model.Love;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LoveMapper {
    /**
     * 收藏
     * @param love
     * @return
     */
    int love(Love love);

    /**
     * 取消收藏
     * @param love
     * @return
     */
    int delLove(Love love);

    /**
     * 查询是否收藏
     * @param love
     * @return
     */
    Love isLove(Love love);

    /**
     * 根据用户id获取小说id集合
     * @param uid
     * @return
     */
    List<Integer> getNidByUid(@Param("uid") Integer uid);
}
