package com.example.demo.mapper;

import com.example.demo.model.AD;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IndexAdMapper {
    /**
     * 查询页面广告
     * @return
     */
    List<AD> ads();
}
