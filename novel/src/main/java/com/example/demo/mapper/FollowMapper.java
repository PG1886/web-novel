package com.example.demo.mapper;

import com.example.demo.model.Follow;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FollowMapper {
    /**
     * 关注
     * @param follow
     * @return
     */
    int follow(Follow follow);

    /**
     * 取关
     * @param follow
     * @return
     */
    int delFollow(Follow follow);

    /**
     * 查询是否关注
     * @param follow
     * @return
     */
    Follow isFollow(Follow follow);
}
