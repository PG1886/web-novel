package com.example.demo.mapper;

import com.example.demo.model.Application;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ApplicationMapper {
    /**
     * 添加作者申请
     * @param application
     * @return
     */
    int add(Application application);
}
