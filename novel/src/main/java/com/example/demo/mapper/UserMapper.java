package com.example.demo.mapper;

import com.example.demo.model.Login;
import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    /**
     * @Author PG
     * 服务于注册接口
     * @param user
     * @return
     */
    int enroll(User user);

    /**
     * 用于登录查询密码接口
     * @param user
     * @return
     */
    User login(User user);

    /**
     * 登录记录
     * @param login
     * @return
     */
    int loginRecord(Login login);

    /**
     * 修改个人信息
     * @param user
     * @return
     */
    int updateInfo(User user);

    /**
     * 关注数量自增
     * @return
     */
    int addFollow(@Param("id") Integer id);

    /**
     * 关注数量自减
     * @param id
     * @return
     */
    int delFollow(@Param("id") Integer id);

    /**
     * 收藏数量自增
     * @param id
     * @return
     */
    int addLikeSize(@Param("id") Integer id);

    /**
     * 收藏数量自减
     * @param id
     * @return
     */
    int delLikeSize(@Param("id") Integer id);
}
