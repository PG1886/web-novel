package com.example.demo.mapper;

import com.example.demo.model.Remark;
import com.example.demo.model.vo.RemarkVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RemarkMapper {
    /**
     * 根据小说id查询用户信息
     * @param nid
     * @return
     */
    List<RemarkVO> remarks(@Param("nid") Integer nid);

    /**
     * 添加评论表
     * @param remark
     * @return
     */
    int remarkNovel(Remark remark);
}
