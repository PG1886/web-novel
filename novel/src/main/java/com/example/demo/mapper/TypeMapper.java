package com.example.demo.mapper;

import com.example.demo.model.Type;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TypeMapper {
    /**
     * 查询导航；类型
     * @return
     */
    List<Type> types();
}
