package com.example.demo.component;

import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.SendEmail;
import com.example.demo.configur.RabbitMQConfig;
import com.example.demo.model.middlewareVO.UserMQ;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.util.StringUtils;

import java.io.IOException;

// 使用@Slf4j注解自动生成日志对象
// 使用@Component注解将该类标识为Spring容器中的组件
@Slf4j
@Component
public class RabbitMQComponent {
    // 注入爬虫数据
    @Autowired
    private Crawler crawler;

    // 自动注入ObjectMapper对象，用于JSON序列化和反序列化
    @Autowired
    private ObjectMapper objectMapper;

    // 自动注入RabbitTemplate对象，用于处理MQ相关
    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 进入延迟队
    public void sendLogin(UserMQ message) {
        send(RabbitMQConfig.DELAY_EMAIL_ROUTING_KEY, message);
    }

    // 进入通知队列
    public void sendNotice(UserMQ message) {
        send(RabbitMQConfig.NOTICE_EMAIL_ROUTING_KEY, message);
    }

    // 添加消息到MQ的通用方法，将消息发送到指定交换机由交换机根据路由分配到指定队列
    @SneakyThrows
    private <T> void send(String routingKey, T message) {
        // 设置消息确认回调函数
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (!ack) {
                // 如果消息发送失败，则重新发送
                send(routingKey, message);
            }
            log.info("消息发送成功");
        });
        // 当消息无法路由时返回
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnsCallback(returnedMessage -> {
            // 如果消息无法路由，则重新发送
            send(routingKey, returnedMessage.getMessage());
        });
        // 将消息内容转化为JSON格式并发送
        String json = objectMapper.writeValueAsString(message);
        rabbitTemplate.convertAndSend(RabbitMQConfig.EMAIL_EXCHANGE, routingKey, json);
    }

    // 监听通知队列
    @SneakyThrows
    @RabbitListener(queues = RabbitMQConfig.NOTICE_EMAIL_QUEUE)
    public void notice(Message message, Channel channel) {
        long tag = message.getMessageProperties().getDeliveryTag();
        try {
            // 处理：通知用户申请的作者权限已审核完成
            UserMQ userMQ = objectMapper.readValue(message.getBody(), UserMQ.class);
            if (userMQ == null
                    || !StringUtils.hasLength(message.getBody().toString())
                    || !SendEmail.isEmail(userMQ.getEmail())
                    || !StringUtils.hasLength(userMQ.getMessage())) {
                log.info("消息发送失败" + userMQ);
            }

//            if (userMQ.getMessage().startsWith("crawler:https://www.qimao5.com/book")) {
//                // 开启异步线程处理爬虫
//                new Thread(){
//                    @Override
//                    public void run() {
//                        try {
//                            crawler.crawlSpecificNovel(userMQ.getMessage().substring(userMQ.getMessage().lastIndexOf(":") + 1));
//                            crawler.insert();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }.start();
//            }

            SendEmail.sendCodeEmail(ApplicationVariable.EMAIL_FROM,userMQ.getEmail(),"novel",userMQ.getMessage());

            // 应答消息已被成功处理
            channel.basicAck(tag, true);
        } catch (Exception e) {
            // 应答消息处理失败，允许重复投递
            channel.basicNack(tag, true, true);
        }
    }

    // 监听延迟队列
    @RabbitListener(queues = RabbitMQConfig.DLX_EMAIL_QUEUE)
    @SneakyThrows
    public void login(Message message, Channel channel) {
        long tag = message.getMessageProperties().getDeliveryTag();
        try {
            // 处理：通知用户创建账户后2天签到
            UserMQ userMQ = objectMapper.readValue(message.getBody(), UserMQ.class);
            if (userMQ == null
                    || !StringUtils.hasLength(message.getBody().toString())
                    || !SendEmail.isEmail(userMQ.getEmail())
                    || !StringUtils.hasLength(userMQ.getMessage())) {
                log.info("消息发送失败" + userMQ);
            }
            // 发送邮件
            SendEmail.sendCodeEmail(ApplicationVariable.EMAIL_FROM, userMQ.getEmail(), "novel", userMQ.getMessage());
            // 应答消息已被成功处理
            channel.basicAck(tag, true);
        } catch (Exception e) {
            // 应答消息处理失败，允许重复投递
            channel.basicNack(tag, true, true);
        }
    }
}