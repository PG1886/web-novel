package com.example.demo.component;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

/**
 * 用户 Session 和 Token 工具类
 */
@Component
public class TokenUtil {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 根据 key 从 HttpSession 中获取对象
     * @param key HttpSession 中的 key
     * @param request HTTP 请求对象
     * @return 对象
     */
    public Object getObjectBySession1(String key, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null || session.getAttribute(key) == null) {
            return null;
        }
        Object object = session.getAttribute(key);
        return object;
    }

    /**
     * 将对象设置到 HttpSession 中
     * @param key HttpSession 中的 key
     * @param object 要设置的对象
     * @param request HTTP 请求对象
     */
    public void setObjectInSession(String key, Object object, HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        session.setAttribute(key, object);
    }

    /**
     * 根据 key 从 HttpSession 中移除对象
     * @param key HttpSession 中的 key
     * @param request HTTP 请求对象
     */
    public void removeObjectInSession(String key, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(key);
    }

    /**
     * 根据 key 从 Redis 中获取对象
     * @param key Redis 中的 key
     * @return 对象
     */
    @SneakyThrows
    public <T> T getObjectByRedis(String key, Class<T> type) {
        String json = stringRedisTemplate.opsForValue().get(key);
        return objectMapper.readValue(json,type);
    }

    /**
     * 将对象设置到 Redis 中
     * @param key Redis 中的 key
     * @param object 要设置的对象
     */
    @SneakyThrows
    public void setObjectInRedis(String key, Object object, Long time, TimeUnit unit) {
        stringRedisTemplate.opsForValue().set(key,objectMapper.writeValueAsString(object),time,unit);
    }

    /**
     * 根据 key 从 Redis 中移除对象
     * @param key Redis 中的 key
     */
    public void removeObjectInRedis(String key) {
        stringRedisTemplate.delete(key);
    }

    public void setRedisKeyTTL(String key,Long time,TimeUnit unit) {
        stringRedisTemplate.expire(key,time,unit);
    }
}

