package com.example.demo.component;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * 分页组件
 */
public class LimitPage {
    public static<T> List<T> limitPage(Integer page, Integer size, Function<List<Integer>,List<T>> service) {
        if (page == null || page <= 0 || size == null || size <= 0) {
            return null;
        }

        int from = (page - 1) * size + 1;
        List<Integer> list = new ArrayList<>();
        list.add(from);
        list.add(size);

        List<T> result = service.apply(list);
        return result;
    }
}
