package com.example.demo.component;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.example.demo.common.RedisDistributedLock;
import com.example.demo.model.middlewareVO.RedisData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * 封装解决redis 缓存击穿、缓存雪崩、缓存穿透等问题的方法
 */
@Component
@Slf4j
public class RedisUtil {
    public static final String FOLLOW_KEY = "FOLLOW:AID:";             // 关注key
    public static final String LOVE_KEY = "LOVE:NID:";                 // 收藏key
    public static final String TOKE_USER_KEY = "TOKEN:USER:";           // 缓存用户信息
    public static final String EMAIL_CODE_KEY = "EMAIL:CODE:";          // 存储验证码的key 后续拼接email
    public static final Long TOKEN_USER_TTL = 30L;                      // token过期时间
    public static final Long EMAIL_CODE_TTL = 1L;                       // 验证码过期时间
    public static final String LIMIT_IP_KEY = "LIMIT:IP:";             // IP
    public static final Long LIMIT_IP_TTL = 1L;                        // 限流时长
    public static final String VISIT_COUNT_KEY = "VISIT:COUNT";  // 访问量
    public static final String TYPES_KEY = "types:version:";     // 类型缓存key
    public static final String CHAPTERS_KEY = "CHAPTER:NID:";    // 小说章节缓存key
    public static final String AD_KEY = "index:ad";              // 主页广告缓存
    public static final Long TTL = 30L;                          // 普通redis key过期时间
    public static final String HOT_USER_KRY = "hot:uid:";        // 推荐的id

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 存入时设置过期时间
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void set(String key, Object value, Long time, TimeUnit unit) throws JsonProcessingException {
        stringRedisTemplate.opsForValue().set(key,objectMapper.writeValueAsString(value),time,unit);
    }

    /**
     * 存入时设置逻辑过期
     * @param key
     * @param value
     * @param time
     * @param unit
     */
    public void setWithLogicalExpire(String key, Object value, Long time, TimeUnit unit) throws JsonProcessingException {
        RedisData data = new RedisData(LocalDateTime.now().plusSeconds(unit.toSeconds(time)),value);
        stringRedisTemplate.opsForValue().set(key,objectMapper.writeValueAsString(data),time,unit);
    }

    /**
     * 查询缓存，缓存穿透时缓存null解决
     * @param id  要查询的id
     * @param keyPrefix   redis key的前缀
     * @param type        要返回的类型
     * @param dbFallback  查询数据库的方法的结果
     * @param <R>
     * @param <I>
     * @return
     * @throws JsonProcessingException
     */
    public<R,I> R getWithPassThrough(I id, String keyPrefix, Class<R> type, Function<I,R> dbFallback,Long time,TimeUnit unit) throws JsonProcessingException {
        // 生成redis中的key
        String key = keyPrefix + id;

        // 查询redis
        String json = stringRedisTemplate.opsForValue().get(key);
        if (StrUtil.isNotBlank(json)) {
            // 存在缓存，则返回
            return objectMapper.readValue(json,type);
        }

        // 不存在时，判断是否是空值
        if (json != null) {
            // 返回错误信息
            return null;
        }

        // 查询数据库
        R result = dbFallback.apply(id);
        // 判空
        if (result == null) {
            // 发送缓存穿透，缓存null
            stringRedisTemplate.opsForValue().set(key,"",time,unit);
            return null;
        }

        // 存在则重建缓存
        stringRedisTemplate.opsForValue().set(key,objectMapper.writeValueAsString(result),time,unit);
        return result;
    }

    /**
     * 查询缓存，逻辑过期后异步重建缓存
     * @param id
     * @param keyPrefix
     * @param type
     * @param dbFallback
     * @param time
     * @param unit
     * @param <R>
     * @param <I>
     * @return
     * @throws JsonProcessingException
     */
    public <R,I> R get(I id, String keyPrefix,Class<R> type,Function<I,R> dbFallback,Long time,TimeUnit unit) throws JsonProcessingException {
        // 构建查询redis的key
        String key = keyPrefix + id;

        // 查询redis
        String json = stringRedisTemplate.opsForValue().get(key);

        // 反序列化转为RedisData对象
        RedisData data = objectMapper.readValue(json,RedisData.class);
        // 获取数据
        R r = JSONUtil.toBean((JSONObject) data.getData(),type);
        // 获取逻辑时间
        LocalDateTime expireTime = data.getExpireTime();

        // 判断是否过期
        if (expireTime.isAfter(LocalDateTime.now())) {
            // 没有过期
            return r;
        }

        // 到达此处已过期，重建缓存：加互斥锁--》查询数据库--》重建--》释放互斥
        // 1.获取互斥锁
        String lockKey = id + "_lock";
        RedisDistributedLock lock = new RedisDistributedLock(lockKey,stringRedisTemplate);
        boolean isLock = lock.tryLock(30);

        if (isLock) {
            // 2.加锁成功，开启线程重建缓存
            new Thread(){
                @Override
                public void run() {
                    try {
                        // 3.查询数据库
                        R db = dbFallback.apply(id);
                        // 4.重建
                        stringRedisTemplate.opsForValue().set(key,objectMapper.writeValueAsString(db),time,unit);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    } finally {
                        // 5.释放互斥锁
                        lock.unlock();
                    }
                }
            }.start();
        }

        // 返回逻辑过期的数据
        return r;
    }

    /** @param key 请求的key
     * @param value 请求的value
     * @param time 时间
     * @param unit 时间的单位
     * @return 如果请求已经被处理过，返回false；否则返回true并将value值存入缓存中
     * @throws JsonProcessingException
     */
    public boolean isRequest(String key, Object value, Long time, TimeUnit unit) throws JsonProcessingException {
        String jsonValue = objectMapper.writeValueAsString(value); // 将请求参数序列化成JSON字符串
        Boolean isAbsent = stringRedisTemplate.opsForValue().setIfAbsent(key, jsonValue, time, unit); // 将请求参数存入缓存
        return Boolean.TRUE.equals(isAbsent); // 如果返回值为true，说明请求未被处理过，否则返回false
    }
}
