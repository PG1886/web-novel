package com.example.demo.mapper;

import com.example.demo.model.Novel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest

class NovelMapperTest {

    @Autowired
    private NovelMapper novelMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void search() {

//        Long add = stringRedisTemplate.opsForSet().add("uid:1", String.valueOf(id));
        Set<String> add = stringRedisTemplate.opsForSet().members("uid:1");
        List<Integer> ids = add.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        System.out.println(ids);
    }

    @Test
    void add() {
        Novel novel = new Novel();
        novel.setNovelName("test");
        novel.setAid(1);
        novel.setPseudonym("test");
        novel.setDescription("test");
        novel.setType(1);
        int re = novelMapper.add(novel);
        System.out.println(re);
        System.out.println(novel.getId());
    }

    @Test
    void getNovelsByIds() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        System.out.println(novelMapper.getNovelsByIds(list));
    }

    @Test
    void queryNovelByIds() {
        System.out.println(stringRedisTemplate.opsForSet().members("tt").isEmpty());
    }
}