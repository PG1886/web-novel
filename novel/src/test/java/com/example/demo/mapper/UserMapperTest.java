package com.example.demo.mapper;

import com.example.demo.component.RedisUtil;
import com.example.demo.component.TokenUtil;
import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.mail.EmailException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@SpringBootTest
@Transactional
class UserMapperTest {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TokenUtil TokenUtil;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UserMapper userMapper;

    @Test
    void updateInfo() throws EmailException, JsonProcessingException {
//
        User user = new User();
        user.setPhoto("kkkk.jpg");
        System.out.println(userMapper.updateInfo(user));
    }
}