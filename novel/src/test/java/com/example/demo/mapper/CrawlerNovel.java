package com.example.demo.mapper;
import com.example.demo.common.ApplicationVariable;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CrawlerNovel {
    public static void crawlerOneChapter(String path) throws IOException {
        String url = "https://www.qimao5.com/book/3312/1.html";
        Connection connect = Jsoup.connect(url);
        Document document = connect.get();
        //经F12分析，章节的标题、内容组合选择器如下所示
        Elements title = document.select(".content h1");
        Elements content = document.select("#chaptercontent");
        //利用IO流将小说章节内容保存到本地
        FileWriter fw = new FileWriter("d:/test/abc.txt");
        fw.write(title.text() + "\r\n");
        for (Element e : content) {
            fw.write(e.text() + "\r\n");
        }
        fw.close();
    }

    public static void main(String[] args) throws IOException {
   ;
        crawler();
    }


    public static void crawlerOneChapter1(String path,String url, Document document) throws IOException {
        Connection connect = Jsoup.connect(url);
        document = connect.get();
        //经F12分析，章节的标题、内容组合选择器如下所示
        Elements title = document.select(".content h1");
        Elements content = document.select("#chaptercontent");
        //利用IO流将小说章节内容保存到本地
        FileWriter fw = new FileWriter(path);
        fw.write(title.text() + "\r\n");
        for (Element e : content) {
            fw.write(e.text() + "\r\n");
        }
        fw.close();
    }

    public static void crawler() throws IOException {
        File dir = new File(ApplicationVariable.CRAWLER_ROOT_PATH);
        dir.mkdirs();

        Document document = null;
        String prefix = "https://www.qimao5.com";

        //小说网站首页
        String url = "https://www.qimao5.com/";
        CloseableHttpClient httpClient = HttpClients.createDefault();

        //访问小说网首页
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = httpClient.execute(httpGet);

        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity entity = response.getEntity();
            String html = EntityUtils.toString(entity, "UTF-8");
            document = Jsoup.parse(html);

            //解析推榜单，得到书籍标题和href
            Elements books = document.select(".wrap .hot .item dl dt span");
            System.out.println("书单"+ books);
            for (org.jsoup.nodes.Element book : books) {
                url = prefix  + book.attr("href");
                System.out.println("书籍主页url:" + url);
                //进入书籍主页
                httpGet = new HttpGet(url);
                response = httpClient.execute(httpGet);
                if (response.getStatusLine().getStatusCode() == 200) {
                    System.out.println("服务器响应成功:" + book);
                    entity = response.getEntity();
                    html = EntityUtils.toString(entity, "UTF-8");
                    document = Jsoup.parse(html);
                    //解析书籍主页中的“免费阅读”按钮，得到书籍第一章的URL
                    url =prefix + document.select(".wrap .hot .item .image a").attr("href") + "/1.html";
                    System.out.println("小说第一章url:" + url);
                    //根据强推榜中的书籍标题，通过IO流，为每一本小说创建文件
                    // 创建目录
//                  FileWriter fw = new FileWriter("d:/test/" + book.text() + ".txt");
                    FileWriter fw = null;

                    //循环读取每一章节的内容，并使用IO流存储到对应的文件中
                    System.out.println(book.text());
                    File file = new File(ApplicationVariable.CRAWLER_ROOT_PATH + book.text());
                    file.mkdir();
                    while (true) {
                        httpGet = new HttpGet(url);
                        response = httpClient.execute(httpGet);
                        if (response.getStatusLine().getStatusCode() == 200) {
                            entity = response.getEntity();
                            //将内容转换成String
                            html = EntityUtils.toString(entity, "UTF-8");
                            document = Jsoup.parse(html);

                            Elements title = document.select(".content h1");
                            Elements content = document.select("#chaptercontent");

                            String path = ApplicationVariable.CRAWLER_ROOT_PATH + book.text()+"/" + title.text() + ".txt";
                            System.out.println(path);
//                            File file1 = new File(path);
//                            file1.createNewFile();
//                            fw = new FileWriter(path);

                            Elements next = document.select("#pb_next");
                            String href = next.attr("href");
                            url = prefix + href;
                            System.out.println("爬取完成，开始下一章，url：" + url);
                            if (url.equals(prefix)) {
                                break;
                            }
//                            fw.write(title.text() + "\r\n");
//                            fw.write(title.text() + "\r\n");
//                            for (Element e : content) {
//                                fw.write(e.text() + "\r\n");
//                            }
                            crawlerOneChapter1(path,url,document);
                        }
                    }
                    fw.close();
                }
            }
        }
    }
}
