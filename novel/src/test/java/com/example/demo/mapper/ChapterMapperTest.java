package com.example.demo.mapper;

import cn.hutool.core.util.StrUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.domain.geo.GeoReference;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class ChapterMapperTest {

    @Autowired
    private ChapterMapper chapterMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void test() {
        // 1.获取用户id
        Long userId = 1L;

        // 2.存入redis
        // 2.1 构建key
        String key = "read:view" + 1;
        // 2.2 存入
        stringRedisTemplate.opsForHyperLogLog().add(key,userId.toString());

        // 3.统计
        Long size = stringRedisTemplate.opsForHyperLogLog().size(key);
        System.out.println(size);
    }

    @Test
    void chaptersByNid() {
    }

    @Test
    void testChaptersByNid() {
        System.out.println(chapterMapper.chaptersByNid(3));
    }
}