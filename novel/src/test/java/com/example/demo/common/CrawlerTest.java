package com.example.demo.common;

import com.example.demo.model.Chapter;
import com.example.demo.model.Novel;
import com.example.demo.service.ChapterService;
import com.example.demo.service.NovelService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CrawlerTest {

    @Autowired
    private NovelService service;

    @Autowired
    private ChapterService chapterService;

    @Test
    void insert() {
        // 思路：将爬虫的小说指定为默认作者 id=1    首先获取子文件信息：写入novel表   其次获取子txt文件信息：写入Chapter表
        // 扫描指定路径
        File root = new File(ApplicationVariable.CRAWLER_ROOT_PATH);
        // 获取子目录
        File[] novels = root.listFiles();
        // 遍历子目录
        for (File novel : novels) {
            if (novel.isDirectory()) {
                // 获取小说信息: 类型、描述、小说名、笔名、作者
                Novel dbNovel = new Novel();
                dbNovel.setNovelName(novel.getName());
                dbNovel.setType(1);
                dbNovel.setAid(1);
                dbNovel.setPseudonym("来源网络");
                dbNovel.setDescription("素材均来自网络、如有侵权立即删除");
                // 写入数据库
                service.add(dbNovel);

                // 获取小说id
                Integer nid = dbNovel.getId();
                System.out.println(nid);
                // 遍历具体章节
                File[] chapters = novel.listFiles();
                for (File chapter : chapters) {
                    // 获取章节信息:标题、内容、nid、aid
                    try(Reader reader = new FileReader(chapter);) {
                        Chapter dbChapter = new Chapter();
                        dbChapter.setNid(nid);
                        dbChapter.setAid(1);
                        dbChapter.setTitle(chapter.getName().replace(".txt",""));
                        StringBuilder content = new StringBuilder();
                        while (true) {
                            int read = reader.read();
                            if (read == -1) {
                                break;
                            }
                            content.append((char) read);
                        }
                        dbChapter.setContent(content.toString());
                        chapterService.add(dbChapter);
                        chapter.delete();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // 删除文件
                novel.delete();
            }
        }
    }
}