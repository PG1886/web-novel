package com.example.demo.component;

import com.example.demo.model.middlewareVO.UserMQ;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
class RabbitMQComponentTest {

    @Autowired
    private RabbitMQComponent rabbitMQComponent;

    @Test
    void sendNotice() {
        UserMQ userMQ = new UserMQ();
        userMQ.setMessage("您的申请已通过，以下是您的账户信息");
        userMQ.setEmail("2194819082@qq.com");
        rabbitMQComponent.sendNotice(userMQ);
    }
}