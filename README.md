# web-novel

#### 介绍
小说网站（小说网站+管理系统）

#### 软件架构
- 1.开发环境：IDEA2021.x、MySQL、Tomcat8.5.85、JDK1.8
- 2.后端：Spring、SpringBoot、SpringMVC、MyBatis、Redis、RabbitMQ、Lua
- 3.前端：以Vue为主


#### 参与贡献

仓库开发者成员

#### 项目说明
1. 小说系统
2. 管理系统

#### 结构图
![输入图片说明](introduce/inreitimage.png)


#### 项目演示
前台主页
![输入图片说明](introduce/newindeximage.png)
前台分类
![输入图片说明](introduce/classifyimage.png)
![输入图片说明](introduce/newnovelindeximage.png)
前台作者专区
![输入图片说明](introduce/applyimage.png)
![输入图片说明](introduce/writeimage.png)
前台个人中心
![输入图片说明](introduce/newlikeimage.png)
![输入图片说明](introduce/userinfoimage.png)
![输入图片说明](introduce/settingsimage.png)
后台管理系统
![输入图片说明](introduce/auh.pngimage.png)
![输入图片说明](introduce/wwqimage.png)
![输入图片说明](introduce/wq2wqimage.png)
![输入图片说明](introduce/wq1edcimage.png)
![输入图片说明](introduce/dsfbimage.png)
![输入图片说明](introduce/feawfaewimage.png)
![输入图片说明](introduce/sdvsdvsdimage.png)



#### 项目启动说明
- 1. 前台前端部署
拉取项目到本地后，将该项目中Vue路径下的novel文件夹在VSCode中打开，新建终端输入

```
npm run serve
```
启动后，启动后端

- 2. 前台后端部署
上述项目中的novel则为后端，在IDEA中打开后，找到db.sql在数据库中执行后找到配置文件application.yml中修改数据库以及RabbitMQ、Redis密码，点击运行即可

#### 遇到的问题
- 1.Vue简单跨域问题：WebMvcConfigurer中addCorsMappings进行配置

```
public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)                  // 允许携带cookie
                .allowedHeaders("*") // 允许跨域请求中包含 token 头部信息
                .allowedMethods("*")
                .allowedOrigins("http://localhost:7070");// 跨域地址
    }
```

- 2.使用Redis进行缓存Type缓存重建时，其他线程盲等问题：采用轮询的办法，其他没有获取到分布式锁的线程轮询去redis查询缓存，直到查到缓存为止，并配置轮询最大次数避免redis宕机缓存重建失败导致线程一直阻塞

```
// 2.4 其他线程加锁失败：等待一会再返回，由于此种情况出现情况极少，仅redis宕机缓存失效，此时等待时长可以接收
                log.info(Thread.currentThread().getName() + "未获取到锁");
                // 2.5 从队列中取出重建的缓存数据并返回
                int count = 20;
                while (true) {
                    String re = stringRedisTemplate.opsForValue().get(RedisUtil.TYPES_KEY + 1);
                    if (StringUtils.hasLength(re)) {
                        types = (List<Type>) objectMapper.readValue(json, new TypeReference<List<Type>>() {});
                        break;
                    }

                    if (count == 0) {
                        break;
                    }
                    count--;
                }
```

- 3.Vue跨域问题中携带token请求头时预请求问题导致拦截器bug问题：解决博客https://blog.csdn.net/qq_61903414/article/details/131048762?spm=1001.2014.3001.5501
- 4.使用RabbitMQ异步处理SQL操作时，消息如何定义以及消费者如何消息问题：待解决
- 5.Vue中图片渲染的问题：将图片上传后保存到assert文化下，后端将图片名保存后，需要时将路径对应的返回文件名即
- 6.前端中当在分类组件时切换其他分类，query变化但是不会重复发送请求：监听页面url的变化，当url变化时，重新发送请求

```
watch: {
    '$route.query': {
      handler(newQuery, oldQuery) {
        if (newQuery.id !== oldQuery.id) {
          // 查询字符串发生变化时重新发送请求
          this.getNovels();
        }
      },
      immediate: true
    }
  },
```
- 7.上传头像时前端无法接收后端响应：

```
            <el-upload
              class="avatar-uploader"
              action="http://127.0.0.1:8080/novel/user/photo"
              :show-file-list="false"
              :on-success="succ"
              :headers="headers"
              :before-upload="beforeAvatarUpload">
              <img v-if="imageUrl" :src="imageUrl" class="avatar">
              <i v-else class="el-icon-plus avatar-uploader-icon"></i>
            </el-upload>
```

```
succ: function(response, file, fileList) {
        console.log(file)
        console.log(fileList)
        const data = response.data; // 从服务器返回的数据中获取上传成功后的头像地址
        this.form.photo = data; // 将上传成功的头像地址保存到data中
          
          localStorage.setItem("photo",response.data)
          console.log(response)  
    },
 
      beforeAvatarUpload(file) {
    
        const isJPG = file.type === 'image/jpeg';
        const isLt2M = file.size / 1024 / 1024 < 2;

        if (!isJPG) {
          this.$message.error('上传头像图片只能是 JPG 格式!');
          return false

        }
        if (!isLt2M) {
          this.$message.error('上传头像图片大小不能超过 2MB!');
          return false
        }
        return true
    }
```

