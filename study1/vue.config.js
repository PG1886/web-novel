const { defineConfig } = require('@vue/cli-service')
const webpack = require('webpack')

module.exports = defineConfig({
transpileDependencies: true,
  devServer: {
    port: 7070,
    proxy: {
      '/api' : {
        target: 'http://localhost:8080',
        changeOrigin: true,
      },
    },

  },
  // chainWebpack: config => {
  //   config.plugin('provide').use(webpack.ProvidePlugin, [{
  //     $: 'jquery',
  //     jquery: 'jquery',
  //     jQuery: 'jquery',
  //     'window.jQuery': 'jquery'
  //   }])
  // }
  
})



 
