import Vue from 'vue'
import VueRouter from 'vue-router'
// import App from '.././App.vue'
import Index from '../views/Layout/MyMenu.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: Index,
    redirect: '/login',
    
  },
  {
    path: '/index/view',
    component: () => import('@/views/Home/Data.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/index',
    redirect: '',
    component: () => import('@/views/Layout/index.vue'),
    children: [
    ],
    meta: {
      requiresAuth: true
    }
  },
  {
    name:"userinfo",
    path: '/index/user/info',
    component: () => import('@/views/User/User.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/index/author/info',
    component: () => import('@/views/author/authorInfo.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/author/application',
    component: () => import('@/views/author/authorApplication.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/book/info',
    component: () => import('@/views/book/bookInfo.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/book/application',
    component: () => import('@/views/book/bookApplication.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/book/get',
    component: () => import('@/views/book/getBook.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/ops/get',
    component: () => import('@/views/ops/getBookRecord.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/ops/login',
    component: () => import('@/views/ops/loginRecord.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/ops/message',
    component: () => import('@/views/ops/comments.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/advice/message',
    component: () => import('@/views/advice/comment.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/index/advice/email',
    component: () => import('@/views/advice/email.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('../views/User/UserLogin.vue')
  },

  {
    path: '*',
    name: '404',
    component: () => import('../views/Home/NotFount.vue')
  }

]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // 判断该路由是否需要登录权限
  if (to.meta.requiresAuth) {
    const isAuthenticated = localStorage.getItem('token');
    if (!isAuthenticated && to.name !== 'login') {
      next({ name: 'login' });
    } else if (isAuthenticated && to.name === 'login') {
      next({ name: 'userinfo' })
    }
    else {
      next();
    }
  } else {
    next();
  }
});
export default router
