// 异常处理拦截器类，用于处理系统异常和业务异常
package com.example.novelrear.intercepter;

import com.example.novelrear.exception.BusinessException;
import com.example.novelrear.exception.SystemException;
import com.example.novelrear.model.Code;
import com.example.novelrear.model.Result;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ProjectExceptionAdvice {
    // 处理系统异常
    @ExceptionHandler(SystemException.class)
    public Result doSystemException(Exception ex){
        System.out.println("有系统异常");
        return new Result(Code.SYSTEM_UNKNOW_ERR,"有系统异常");
    }
    // 处理业务异常
    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(Exception ex){
        System.out.println("有事务异常");
        return new Result(Code.PROJECT_BUSINESS_ERR,"有事务异常");
    }
    // 处理其他异常
    @ExceptionHandler(Exception.class)
    public Result doException(Exception ex){
        System.out.println("其他有异常" + ex.getMessage());
        return new Result(Code.SYSTEM_UNKNOW_ERR,"其他有异常");
    }
}
