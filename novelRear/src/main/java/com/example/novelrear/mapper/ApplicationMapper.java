package com.example.novelrear.mapper;

import com.example.novelrear.model.Application;
import com.example.novelrear.model.Author;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ApplicationMapper {
    public List<Application> selectAll();
    public Boolean apply(Integer id,Integer state);
}
