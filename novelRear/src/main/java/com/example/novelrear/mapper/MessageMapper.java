package com.example.novelrear.mapper;

import com.example.novelrear.model.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageMapper {

    int add(Message message);

    List<Message> selectAll();

    Boolean delete(int id);

    Message get(int id);

    Boolean update(Integer id,String message);
}
