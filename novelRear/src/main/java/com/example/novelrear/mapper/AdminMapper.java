package com.example.novelrear.mapper;

import com.example.novelrear.model.Admin;
import com.example.novelrear.model.Admin;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminMapper {
    public int save(Admin admin);

    public int delete(Integer id);

    public int update(Admin admin);

    public Admin selectById(Integer id);

    public List<Admin> selectAll();

    public int selectCount();

    public List<Admin> selectByPage(Integer count,Integer begin);
    public String login(String username);
}
