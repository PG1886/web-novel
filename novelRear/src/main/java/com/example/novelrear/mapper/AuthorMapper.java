package com.example.novelrear.mapper;

import com.example.novelrear.model.Author;
import com.example.novelrear.model.Author;
import com.example.novelrear.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AuthorMapper {
    public Integer save(Author author);

    public Integer delete(Integer id);

    public Integer update(Author author);

    public Author selectById(Integer id);

    public List<Author> selectAll();

    public Integer selectCount();

    public List<Author> selectByPage(Integer count,Integer begin);

    public List<Author> selectByPageAndCondition(Integer count, Integer begin, String nickname);

    public Integer selectCountByPageAndCondition(String nickname);
}
