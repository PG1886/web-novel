package com.example.novelrear.mapper;

import com.example.novelrear.model.Novel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NovelMapper {
    public Boolean save(Novel novel);

    public Boolean delete(Integer id);

    public Boolean update(Novel novel);

    public Novel selectById(Integer id);

    public List<Novel> selectAll();

    public int selectCount();

    public List<Novel> selectByPage(Integer count,Integer begin);
}
