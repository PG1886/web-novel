package com.example.novelrear.mapper;

import com.example.novelrear.model.Login;
import com.example.novelrear.model.PageBean;
import com.example.novelrear.model.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    public Integer save(User user);

    public Integer delete(Integer id);

    public Integer update(User user);

    public User selectById(Integer id);

    public List<User> selectAll();

    public Integer selectCount();

    public List<User> selectByPage(Integer count,Integer begin);

    public List<User> selectByPageAndCondition(Integer count,Integer begin,String nickname);

    public Integer selectCountByPageAndCondition(String nickname);

    public  int deleteByIds(int[] ids);

    public String login(String username);

    public List<Login> loginRecord();

    public Integer applyOk(int id);
}
