package com.example.novelrear.model;

import lombok.Data;

@Data
public class Message {
    private Integer id;
    private String message;;
}
