package com.example.novelrear.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 此类与数据库字段相同:
 */

@Data
public class User implements Serializable {
    private Integer id;
    private String username;//账号
    private String nickname;
    private String password;//密码
    private String photo;
    private int likeSize;
    private String email;
    private int follow;
    private int state;
    private String ip;

}
