package com.example.novelrear.model;

import lombok.Data;

import java.util.List;
@Data
public class PageBean<T> {
    private Integer totalCount;
    private List<T> rows;

    public PageBean(Integer totalCount, List<T> rows) {
        this.totalCount = totalCount;
        this.rows = rows;
    }
}
