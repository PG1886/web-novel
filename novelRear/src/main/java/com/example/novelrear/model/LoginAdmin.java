package com.example.novelrear.model;

import lombok.Data;

@Data
public class LoginAdmin {
    private String username ;           //管理员登录账户

    private String password;            //管理员登录密码
}
