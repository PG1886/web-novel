package com.example.novelrear.model;

import lombok.Data;

@Data
public class Notice {
    private Integer id;
    private String src;
    private int state;
}
