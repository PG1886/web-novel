package com.example.novelrear.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Novel implements Serializable {
    private int id;  //小说ID
    private String novelName;   // 小说书名
    private String pseudonym;    // 作者笔名
    private int aid;              // 作者id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  // 格式化时间
    private LocalDateTime updateTime;
    private int readCount;         // 阅读次数
    private int upvote;            // 点赞次数
    // TODO：推荐将int类型修改为包装类
    private Integer type;           // 小说类型
   private int state;               // 小说状态
    private String photo;

    private String description;
}
