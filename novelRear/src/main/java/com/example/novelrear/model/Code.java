package com.example.novelrear.model;

public class Code {
    public static final Integer SAVE_OK = 200;
    public static final Integer UPDATA_OK = 200;
    public static final Integer DELETE_OK = 200;
    public static final Integer SELECT_OK = 200;

    public static final Integer SAVE_ERR = -1;
    public static final Integer UPDATA_ERR = -1;
    public static final Integer DELETE_ERR = -1;
    public static final Integer SELECT_ERR = -1;

    public static final Integer SYSTEM_UNKNOW_ERR = 500;
    public static final Integer SYSTEM_TIMEOUT_ERR = 50002;
    public static final Integer PROJECT_VALIDATE_ERR = 60001;
    public static final Integer PROJECT_BUSINESS_ERR = 500;
}
