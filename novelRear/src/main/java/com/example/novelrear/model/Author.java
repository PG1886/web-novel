package com.example.novelrear.model;

import lombok.Data;

@Data
public class Author {
    private Integer id; //作者id
    private String username; // 作者登录账户
    private String nickname;//用户客户端昵称
    private String password;// 作者登录密码
    private Integer empower; // 授权的管理员id
    private Integer uid;
    private Integer fan; //粉丝数
    private String ip ;// 登录ip
    private Integer state; // 状态码
}
