package com.example.novelrear.model.VO;

import com.example.novelrear.model.User;
import lombok.Data;

@Data
public class UserMQ extends User {
    private String message;
}
