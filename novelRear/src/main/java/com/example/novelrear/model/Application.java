package com.example.novelrear.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Application {
    private int id;
    private int uid;
    private String nickname;
    private int sex;//0男1女
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")  // 格式化时间
    private LocalDateTime startTime;
    private int state;//0正常 1 审核完毕  2审核失败  3加速审核
}
