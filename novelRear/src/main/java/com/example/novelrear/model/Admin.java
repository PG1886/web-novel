package com.example.novelrear.model;

import lombok.Data;

@Data
public class Admin {
    private int id;

    private String nickname;
    private String username ;           //管理员登录账户
    private String password;            //管理员登录密码
    private String ip ;
}
