package com.example.novelrear.config;

import com.example.novelrear.intercepter.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class SpringMvcConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 添加拦截规则
         */
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")                             // 先拦截所有
                /*.excludePathPatterns("/user/*")
                .excludePathPatterns("/author/*")
                .excludePathPatterns("/admin/*")
                .excludePathPatterns("/common/*")
                .excludePathPatterns("/message/*")
                .excludePathPatterns("/novel/*")*/
                .excludePathPatterns("/admin/login")       ;          // 排除登录接口
    }


    /**
     * 解决前端跨域问题
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)                  // 允许携带cookie
                .allowedHeaders("*") // 允许跨域请求中包含 token 头部信息
                .allowedMethods("*")
                .allowedOrigins("http://localhost:7070");// 跨域地址
    }
}
