package com.example.novelrear.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class RabbitMQConfig {
    // 死信队列 与 交换机
    public static final String DLX_EMAIL_QUEUE = "DLX_EMAIL_QUEUE";           // 邮件死信队列
    private static final String DLX_EMAIL_EXCHANGE = "DLX_EMAIL_EXCHANGE";    // 邮件死信交换机
    private static final String DLX_EMAIL_KEY = "login-email";                // 邮件死信队列路由

    // 延迟队列 与 交换机
    private static final String DELAY_EMAIL_QUEUE = "LOGIN_EMAIL_QUEUE";      // 延迟队列
    public static final String DELAY_EMAIL_ROUTING_KEY = "email.login";       // 延迟队列路由
    private static final int TTL_TIME = (int) TimeUnit.DAYS.toMillis(2);// 2day  延迟队列时间

    // 通知队列 与 交换机
    public static final String NOTICE_EMAIL_QUEUE = "NOTICE_EMAIL_QUEUE";    // 通知队列
    public static final String NOTICE_EMAIL_ROUTING_KEY = "email.notice";    // 通知队列路由
    public static final String EMAIL_EXCHANGE = "EMAIL_EXCHANGE";            // 总交换机

    // 普通队列与交换机
    @Bean
    public Queue login() {
        // 设置队列名，并设置该队列的TTL时间，当消息在该队列中存活TTL_TIME时间后未被消费，则会进入死信队列
        return QueueBuilder.durable(DELAY_EMAIL_QUEUE).ttl(TTL_TIME)
                // 设置该队列的死信交换机
                .deadLetterExchange(DLX_EMAIL_EXCHANGE)
                // 设置该队列的死信路由键
                .deadLetterRoutingKey(DLX_EMAIL_KEY)
                .build();
    }

    @Bean
    public Queue notice() {
        return QueueBuilder.durable(NOTICE_EMAIL_QUEUE).build();
    }

    @Bean
    public TopicExchange email() {
        // 创建主题交换机，并设置持久化
        return ExchangeBuilder.topicExchange(EMAIL_EXCHANGE).durable(true).build();
    }

    // 绑定普通队列与交换机
    @Bean
    public Binding normal1(@Qualifier("notice") Queue notice,@Qualifier("email") TopicExchange email) {
        return BindingBuilder.bind(notice).to(email).with(NOTICE_EMAIL_ROUTING_KEY);
    }

    @Bean
    public Binding normal2(@Qualifier("login") Queue login,@Qualifier("email") TopicExchange email) {
        return BindingBuilder.bind(login).to(email).with(DELAY_EMAIL_ROUTING_KEY);
    }

    // 死信队列与交换机
    @Bean
    public Queue dlxQueue() {
        // 创建死信队列，并设置持久化
        return QueueBuilder.durable(DLX_EMAIL_QUEUE).build();
    }

    @Bean
    public DirectExchange dlxExchange() {
        // 创建直连交换机，并设置持久化
        return ExchangeBuilder.directExchange(DLX_EMAIL_EXCHANGE).durable(true).build();
    }

    // 绑定死信队列与交换机
    @Bean
    public Binding dlxBind(@Qualifier("dlxQueue") Queue dlxQueue,@Qualifier("dlxExchange") DirectExchange dlxExchange) {
        return BindingBuilder.bind(dlxQueue).to(dlxExchange).with(DLX_EMAIL_KEY);
    }
}
