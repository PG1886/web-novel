package com.example.novelrear.controller;

import com.example.novelrear.model.*;
import com.example.novelrear.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin(value = "http://localhost:7070",allowCredentials = "true")
public class UserController {
    @Autowired
    private UserService userService;
    //新增
    @PostMapping("/save")
    public Boolean save(@RequestBody User user){
        return userService.save(user);
    }
    //删除
    @GetMapping("/delete")
    public Result delete(Integer id) {
        Boolean delete = userService.delete(id);
        Integer code  = delete!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  delete!=null?"":"查询出错";
        return new Result(delete,code,msg);
    }
    //修改
    @PostMapping("/update")
    public Result update(@RequestBody User user) {
        System.out.println(user);
        Boolean update = userService.update(user);
        Integer code  = update!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  update!=null?"":"查询出错";
        return new Result(update,code,msg);
    }
    //根据id查询
    @GetMapping("/get")
    public Result selectById(Integer id) {
        User user = userService.selectById(id);
        Integer code  = user!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  user!=null?"":"查询出错";
        return new Result(user,code,msg);
    }
    //查询全部
    @GetMapping("/getall")
    public List<User> selectAll() {
        List<User> users = userService.selectAll();
        Integer code  = users!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  users!=null?"":"查询出错";
        //return (List<User>) new Result(users,code,msg);
        System.out.println("user");
        return  users;
    }
    //分页查询
    @GetMapping("/list")
    public Result SelectByPage(int currentPage, int pageSize){
        List<User> users = userService.selectByPage(currentPage,pageSize);
        int count = userService.selectCount();
        Integer code  = users!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  users!=null?"":"查询出错";
        return  new Result(new PageBean(count, users),code,msg);
    }
    //批量删除
    @PostMapping("/deleteMore")
    public Boolean deleteByIds(@RequestBody int[] ids){
        return userService.deleteByIds(ids);
    }
    //模糊条件分页查询
    @GetMapping("/list1")
    public PageBean selectByPageAndCondition( int currentPage,  int pageSize, String nickname){
        if(nickname!=null&&nickname.length()>0) {
            nickname = "%" + nickname + "%";
        }
        List<User> users = userService.selectByPageAndCondition(currentPage, pageSize, nickname);
        int count = userService.selectCountByPageAndCondition(nickname);
        PageBean pageBean = new PageBean(count,users);
        return  pageBean;
    }



    @GetMapping("/loginRecord")
    public Result loginRecord(){
        List<Login> logins = userService.loginRecord();
        Integer code  = logins!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  logins!=null?"":"查询出错";
        return new Result(logins,code,msg);
    }
}
