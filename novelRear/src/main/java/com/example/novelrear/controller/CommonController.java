package com.example.novelrear.controller;

import com.example.novelrear.config.RabbitMQConfig;
import com.example.novelrear.model.User;
import com.example.novelrear.model.VO.UserMQ;
import com.example.novelrear.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/common")
@CrossOrigin(value = "http://localhost:7070",allowCredentials = "true")
public class CommonController {

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @SneakyThrows
    @RequestMapping("/email")
    public Boolean email(String email) {
        // 发送邮件
        List<User> users = userService.selectAll();
        for (int i = 0; i < users.size(); i++) {
            UserMQ userMQ = new UserMQ();
            userMQ.setEmail(users.get(i).getEmail());
            userMQ.setMessage(email);
            rabbitTemplate.convertAndSend(RabbitMQConfig.EMAIL_EXCHANGE,RabbitMQConfig.NOTICE_EMAIL_ROUTING_KEY,objectMapper.writeValueAsString(userMQ));
        }
        return true;
    }
}
