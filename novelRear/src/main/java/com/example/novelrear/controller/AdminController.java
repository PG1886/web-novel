//管理员控制器 
package com.example.novelrear.controller;

import com.example.novelrear.mapper.AdminMapper;
import com.example.novelrear.model.*;

import com.example.novelrear.service.AdminService;
import com.example.novelrear.service.impl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/admin")
@CrossOrigin(value = "http://localhost:7070",allowCredentials= "true")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminMapper adminMapper;

    //新增管理员 
    @PostMapping("/save")
    public Boolean save(@RequestBody Admin admin){
        return adminService.save(admin);
    }

    //删除管理员 
    @GetMapping("/delete")
    public Boolean delete(Integer id) {
        return adminService.delete(id);
    }

    //修改管理员信息 
    @PostMapping("/update")
    public Boolean update(@RequestBody Admin admin) {
        return adminService.update(admin);
    }

    //根据id查询管理员信息 
    @GetMapping("/get")
    public Result selectById(Integer id) {
        Admin admin = adminService.selectById(id);
        Integer code  = admin !=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  admin !=null?"":"查询出错";
        return new Result(admin,code,msg);
    }

    //查询全部管理员信息 
    @GetMapping("/getall")
    public Result selectAll() {
        List<Admin> admins = adminService.selectAll();
        Integer code  = admins!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  admins!=null?"":"查询出错";
        return  new Result(admins,code,msg);
    }

    //分页查询管理员信息 
    @GetMapping("/page")
    public PageBean SelectByPage(int currentPage, int pageSize){
        List<Admin> admins = adminService.selectByPage(currentPage,pageSize);
        int count = adminService.selectCount();
        return new PageBean(count, admins);
    }

    //管理员登录 
    @GetMapping("/login")
    public Result login(HttpServletRequest request,String username,String password){
        Integer code=-1;
        String realPassword = adminService.login(username);
        if(realPassword.equals(password)){
            HttpSession session=request.getSession(true);//创建session 
            LoginAdmin loginAdmin =  new LoginAdmin();
            loginAdmin.setUsername(username);
            loginAdmin.setPassword(password);
            session.setAttribute("user",loginAdmin);
            System.out.println(session.getAttribute("user"));
            code=200;
        }
        String token = UUID.randomUUID().toString();
        return new Result(token,code,"");
    }
}