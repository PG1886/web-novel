package com.example.novelrear.controller;


import com.example.novelrear.mapper.MessageMapper;
import com.example.novelrear.model.Code;
import com.example.novelrear.model.Message;
import com.example.novelrear.model.Result;
import com.example.novelrear.service.impl.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping("/message")
@CrossOrigin(value = "http://localhost:7070",allowCredentials = "true")
@ResponseBody
@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageMapper messageMapper;

    @RequestMapping("/add")
    public Boolean add(Message message) {
        System.out.println(message);
        return messageService.add(message) == 1;
    }

    @GetMapping("/getall")
    public Result selectAll(){
        List<Message> messages = messageService.selectAll();
        Integer code  = messages!=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  messages!=null?"":"查询出错";
        return  new Result(messages,code,msg);
    }

    @GetMapping("/delete")
    public Result delete(Integer id) {
        Boolean delete = messageMapper.delete(id);
        Integer code  = delete!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  delete!=null?"":"查询出错";
        return new Result(delete,code,msg);
    }
    @GetMapping("/get")
    public Result select(Integer id){
        Message message = messageMapper.get(id);
        Integer code  = message!=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  message!=null?"":"查询出错";
        return  new Result(message,code,msg);
    }
    @GetMapping("/update")
    public Result update(Integer id,String message){
        Boolean update = messageMapper.update(id, message);
        Integer code  = update!=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  update!=null?"":"查询出错";
        return  new Result(update,code,msg);
    }
}
