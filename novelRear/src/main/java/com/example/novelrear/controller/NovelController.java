package com.example.novelrear.controller;

import com.example.novelrear.model.Code;
import com.example.novelrear.model.Novel;


import com.example.novelrear.model.PageBean;
import com.example.novelrear.model.Result;
import com.example.novelrear.service.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/novel")
@CrossOrigin(value = "http://localhost:7070",allowCredentials = "true")
public class NovelController {
    @Autowired
    private NovelService novelService;
    //新增
    @PostMapping("/save")
    public Boolean save(@RequestBody Novel novel){
        return novelService.save(novel);
    }
    //删除
    @GetMapping("/delete")
    public Boolean delete(Integer id) {
        return novelService.delete(id);
    }
    //修改
    @PostMapping("/update")
    public Boolean update(@RequestBody Novel novel) {
        return novelService.update(novel);
    }
    //根据id查询
    @GetMapping("/get")
    public Result selectById(Integer id) {
        Novel novel = novelService.selectById(id);
        Integer code  = novel !=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  novel !=null?"":"查询出错";
        return new Result(novel,code,msg);
    }
    //查询全部
    @GetMapping("/getall")
    public Result selectAll() {
        List<Novel> novels = novelService.selectAll();
        Integer code  = novels!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  novels!=null?"":"查询出错";
        return new Result(novels,code,msg);
    }
    //分页查询
    @GetMapping("/list")
    public Result SelectByPage(int currentPage, int pageSize){
        List<Novel> novels = novelService.selectByPage(currentPage,pageSize);
        int count = novelService.selectCount();
        Integer code  = novels!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  novels!=null?"":"查询出错";
        return new Result(new PageBean(count, novels),code,msg);
    }
}
