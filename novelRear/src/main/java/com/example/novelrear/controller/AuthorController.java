package com.example.novelrear.controller;

import com.example.novelrear.mapper.UserMapper;
import com.example.novelrear.model.*;
import com.example.novelrear.service.ApplicationService;
import com.example.novelrear.service.AuthorService;

import com.example.novelrear.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/author")
@CrossOrigin(value = "http://localhost:7070",allowCredentials= "true")
public class AuthorController {
    @Autowired
    private AuthorService authorService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    // 新增作者
    @PostMapping("/save")
    public Boolean save(@RequestBody Author author){
        return authorService.save(author);
    }

    // 删除作者
    @GetMapping("/delete")
    public Result delete(Integer id) {
        Boolean delete = authorService.delete(id);
        Integer code  = delete!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  delete!=null?"":"查询出错";
        return new Result(delete,code,msg);
    }

    // 修改作者信息
    @PostMapping("/update")
    public Result update(@RequestBody Author author) {
        Boolean update = authorService.update(author);
        Integer code  = update!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  update!=null?"":"查询出错";
        return new Result(update,code,msg);
    }

    // 根据id查询作者信息
    @GetMapping("/get")
    public Result selectById(Integer id) {
        Author author = authorService.selectById(id);
        Integer code  = author !=null? Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  author !=null?"":"查询出错";
        return new Result(author,code,msg);
    }

    // 查询全部作者信息
    @GetMapping("/getall")
    public Result selectAll() {
        List<Author> authors = authorService.selectAll();
        Integer code  = authors!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  authors!=null?"":"查询出错";
        return  new Result(authors,code,msg);
    }

    // 分页查询作者信息
    @GetMapping("/list")
    public Result SelectByPage(int currentPage, int pageSize){
        List<Author> authors = authorService.selectByPage(currentPage,pageSize);
        int count = authorService.selectCount();
        Integer code  = authors!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  authors!=null?"":"查询出错";
        return  new Result(new PageBean(count, authors),code,msg);
    }

    // 根据条件分页查询作者信息
    @GetMapping("/list1")
    public PageBean selectByPageAndCondition( int currentPage,  int pageSize, String nickname){
        if(nickname!=null&&nickname.length()>0) {
            nickname = "%" + nickname + "%";
        }
        List<Author> authors = authorService.selectByPageAndCondition(currentPage, pageSize, nickname);
        int count = authorService.selectCountByPageAndCondition(nickname);
        PageBean pageBean = new PageBean(count,authors);
        return  pageBean;
    }

    // 查询所有申请信息
    @GetMapping("/application")
    public Result selectAllApplication(){
        List<Application> applications = applicationService.selectAll();
        Integer code  = applications!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  applications!=null?"":"查询出错";
        return  new Result(applications,code,msg);
    }

    // 审核申请
    @GetMapping("/apply")
    public Result apply(int id,int state){
        Boolean apply = applicationService.apply(id, state);
        Integer code  = apply!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  apply!=null?"":"修改出错";
        return new Result("",code,msg);
    }

    // 审核通过
    @GetMapping("/applyOk")
    public Result applyOk(int id,int state,int uid){
        Boolean apply = applicationService.apply(id, state);
        User user = userService.selectById(uid);
        userMapper.applyOk(uid);
        Author author = new Author();
        author.setUsername(user.getUsername());
        author.setPassword(user.getPassword());
        author.setEmpower(1);
        author.setUid(uid);
        author.setFan(0);
        author.setNickname("作者886");
        author.setState(0);
        author.setIp("127.0.0.1");
        authorService.save(author);
        Integer code  = apply!=null?Code.SELECT_OK:Code.SELECT_ERR;
        String msg =  apply!=null?"":"修改出错";
        return new Result("",code,msg);
    }
}