package com.example.novelrear.service.impl;

import com.example.novelrear.exception.BusinessException;
import com.example.novelrear.mapper.AuthorMapper;
import com.example.novelrear.model.Author;

import com.example.novelrear.model.Code;
import com.example.novelrear.model.User;
import com.example.novelrear.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorMapper authorMapper;
    @Override
    public Boolean save(Author author) {
        authorMapper.save(author);
        return true;
    }

    @Override
    public Boolean delete(Integer id) {
        authorMapper.delete(id);
        return true;
    }

    @Override
    public Boolean update(Author author) {
        authorMapper.update(author);
        return true;
    }

    @Override
    public Author selectById(Integer id) {
        if (id <= 0) {
            throw new BusinessException(Code.PROJECT_BUSINESS_ERR,"请勿进行非法操作");//防止id小于0
        }
        return authorMapper.selectById(id);
    }

    @Override
    public List<Author> selectAll() {
        return authorMapper.selectAll();
    }


    @Override
    public List<Author> selectByPage(int currentPage,int pageSize) {
        int count = pageSize;
        int begin = (currentPage-1)*count+1;
        int end = currentPage*count;
        return authorMapper.selectByPage(count,begin);
    }

    @Override
    public Integer selectCount() {
        return authorMapper.selectCount();
    }

    @Override
    public List<Author> selectByPageAndCondition(int currentPage, int pageSize, String nickname) {
        int count = pageSize;
        int begin = (currentPage-1)*count;
        List<Author> authors = authorMapper.selectByPageAndCondition(count, begin, nickname);
        return authors;
    }

    @Override
    public Integer selectCountByPageAndCondition(String nickname) {
        return authorMapper.selectCountByPageAndCondition(nickname);
    }
}
