package com.example.novelrear.service;

import com.example.novelrear.model.Author;
import com.example.novelrear.model.User;

import java.util.List;

public interface AuthorService {
    public Boolean save(Author author);

    public Boolean delete(Integer id);

    public Boolean update(Author author);

    public Author selectById(Integer id);

    public List<Author> selectAll();

    public List<Author> selectByPage(int currentPage,int pageSize);

    public Integer selectCount();
    public List<Author> selectByPageAndCondition(int currentPage, int pageSize, String nickname);

    public Integer selectCountByPageAndCondition(String nickname);
}
