package com.example.novelrear.service;

import com.example.novelrear.model.Admin;

import java.util.List;

public interface AdminService {
    public Boolean save(Admin admin);

    public Boolean delete(Integer id);

    public Boolean update(Admin admin);

    public Admin selectById(Integer id);

    public List<Admin> selectAll();

    public List<Admin> selectByPage(int currentPage,int pageSize);

    public Integer selectCount();
    public String login(String username);
}
