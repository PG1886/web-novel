package com.example.novelrear.service.impl;

import com.example.novelrear.exception.BusinessException;
import com.example.novelrear.mapper.AdminMapper;
import com.example.novelrear.model.Admin;
import com.example.novelrear.model.Code;
import com.example.novelrear.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Boolean save(Admin admin) {
        adminMapper.save(admin);
        return true;
    }

    @Override
    public Boolean delete(Integer id) {
        adminMapper.delete(id);
        return true;
    }

    @Override
    public Boolean update(Admin admin) {
        adminMapper.update(admin);
        return true;
    }

    @Override
    public Admin selectById(Integer id) {
        if (id <= 0) {
            throw new BusinessException(Code.PROJECT_BUSINESS_ERR,"请勿进行非法操作");
        }
        return adminMapper.selectById(id);
    }

    @Override
    public List<Admin> selectAll() {
        return adminMapper.selectAll();
    }


    @Override
    public List<Admin> selectByPage(int currentPage,int pageSize) {
        int count = pageSize;           //算出每页行数
        int begin = (currentPage-1)*count;    //算出要查询的数据的开始
        int end = currentPage*count;
        return adminMapper.selectByPage(count,begin);
    }

    @Override
    public Integer selectCount() {
        return adminMapper.selectCount();
    }

    @Override
    public String login(String username) {
        return adminMapper.login(username);
    }
}
