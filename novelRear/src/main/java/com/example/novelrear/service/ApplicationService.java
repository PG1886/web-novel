package com.example.novelrear.service;

import com.example.novelrear.model.Application;
import com.example.novelrear.model.Author;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface ApplicationService {
    public List<Application> selectAll();
    public Boolean apply(Integer id,Integer state);
}
