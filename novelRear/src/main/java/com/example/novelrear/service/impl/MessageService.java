package com.example.novelrear.service.impl;

import com.example.novelrear.mapper.MessageMapper;
import com.example.novelrear.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageMapper messageMapper;

    public int add(Message message) {
        return messageMapper.add(message);
    }

    public List<Message> selectAll(){
        return  messageMapper.selectAll();
    }
}
