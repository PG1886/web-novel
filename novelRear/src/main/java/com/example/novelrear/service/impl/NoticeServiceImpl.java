package com.example.novelrear.service.impl;

import com.example.novelrear.mapper.NoticeMapper;
import com.example.novelrear.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public List<String> selectAll() {
        return noticeMapper.selectAll();
    }
}
