package com.example.novelrear.service.impl;

import com.example.novelrear.mapper.ApplicationMapper;
import com.example.novelrear.model.Application;
import com.example.novelrear.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ApplicationServiceImpl implements ApplicationService {
    @Autowired
    ApplicationMapper applicationMapper;
    @Override
    public List<Application> selectAll() {
        return applicationMapper.selectAll();
    }

    @Override
    public Boolean apply(Integer id, Integer state) {
        applicationMapper.apply(id,state);
        return true;
    }


}
