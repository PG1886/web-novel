package com.example.novelrear.service.impl;

import com.example.novelrear.exception.BusinessException;
import com.example.novelrear.mapper.NovelMapper;
import com.example.novelrear.model.Code;
import com.example.novelrear.model.Novel;

import com.example.novelrear.service.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class NovelServiceImpl implements NovelService {
    @Autowired
    private NovelMapper novelMapper;
    @Override
    public Boolean save(Novel novel) {
        novelMapper.save(novel);
        return true;
    }

    @Override
    public Boolean delete(Integer id) {
        novelMapper.delete(id);
        return true;
    }

    @Override
    public Boolean update(Novel novel) {
        novelMapper.update(novel);
        return true;
    }

    @Override
    public Novel selectById(Integer id) {
        if (id <= 0) {
            throw new BusinessException(Code.PROJECT_BUSINESS_ERR,"请勿进行非法操作");
        }
        return novelMapper.selectById(id);
    }

    @Override
    public List<Novel> selectAll() {
        return novelMapper.selectAll();
    }

    @Override
    public List<Novel> selectByPage(int currentPage, int pageSize) {
        int count = pageSize;
        int begin = (currentPage-1)*count+1;
        int end = currentPage*count;
        return novelMapper.selectByPage(count,begin);
    }

    @Override
    public Integer selectCount() {
        return novelMapper.selectCount();
    }
}
