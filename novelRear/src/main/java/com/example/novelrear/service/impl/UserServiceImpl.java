package com.example.novelrear.service.impl;

import com.example.novelrear.exception.BusinessException;
import com.example.novelrear.mapper.UserMapper;
import com.example.novelrear.model.Code;
import com.example.novelrear.model.Login;
import com.example.novelrear.model.PageBean;
import com.example.novelrear.model.User;
import com.example.novelrear.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public Boolean save(User user) {
        userMapper.save(user);
        return true;
    }

    @Override
    public Boolean delete(Integer id) {
        userMapper.delete(id);
        return true;
    }

    @Override
    public Boolean update(User user) {
        userMapper.update(user);
        return true;
    }

    @Override
    public User selectById(Integer id) {
        if (id <= 0) {
            throw new BusinessException(Code.PROJECT_BUSINESS_ERR,"请勿进行非法操作");
        }
        return userMapper.selectById(id);
    }

    @Override
    public List<User> selectAll() {
        return userMapper.selectAll();
    }


    @Override
    public List<User> selectByPage(int currentPage,int pageSize) {
        int count = pageSize;
        int begin = (currentPage-1)*count+1;
        return userMapper.selectByPage(count,begin);
    }

    @Override
    public Integer selectCount() {
        return userMapper.selectCount();
    }

    @Override
    public List<User> selectByPageAndCondition(int currentPage,int pageSize,String nickname) {
        int count = pageSize;
        int begin = (currentPage-1)*count;
        List<User> Users = userMapper.selectByPageAndCondition(count, begin,nickname);
        return Users;
    }

    @Override
    public Integer selectCountByPageAndCondition(String nickname) {
        return userMapper.selectCountByPageAndCondition(nickname);
    }

    @Override
    public Boolean deleteByIds(int[] ids) {
        userMapper.deleteByIds(ids);
        return true;
    }

    @Override
    public String login(String username) {
        return userMapper.login(username);
    }

    @Override
    public List<Login> loginRecord() {

        return userMapper.loginRecord();
    }
}
