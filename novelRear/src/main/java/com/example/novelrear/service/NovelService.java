package com.example.novelrear.service;

import com.example.novelrear.model.Novel;

import java.util.List;

public interface NovelService {
    public Boolean save(Novel novel);

    public Boolean delete(Integer id);

    public Boolean update(Novel novel);

    public Novel selectById(Integer id);

    public List<Novel> selectAll();

    public List<Novel> selectByPage(int currentPage,int pageSize);

    public Integer selectCount();
}
