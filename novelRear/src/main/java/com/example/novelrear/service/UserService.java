package com.example.novelrear.service;

import com.example.novelrear.model.Login;
import com.example.novelrear.model.PageBean;
import com.example.novelrear.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserService {
    public Boolean save(User user);

    public Boolean delete(Integer id);

    public Boolean update(User user);

    public User selectById(Integer id);

    public List<User> selectAll();

    public List<User> selectByPage(int currentPage,int pageSize);

    public Integer selectCount();

    public List<User> selectByPageAndCondition(int currentPage,int pageSize,String nickname);

    public Integer selectCountByPageAndCondition(String nickname);

    public Boolean deleteByIds(int[] ids);

    public String login(String username);
    public List<Login> loginRecord();
}
